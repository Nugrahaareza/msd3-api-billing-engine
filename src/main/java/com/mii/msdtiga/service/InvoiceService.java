package com.mii.msdtiga.service;

import com.mii.msdtiga.entity.*;
import com.mii.msdtiga.repository.BillingSubscriptionLogRepository;
import com.mii.msdtiga.repository.InvoiceRepository;
import com.mii.msdtiga.repository.OrganizationRepository;
import com.mii.msdtiga.util.Utility;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.IOException;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

@Service
public class InvoiceService {

    static Logger log = LoggerFactory.getLogger(InvoiceService.class);

    public final static String INVOICE_SERVICE = "INVOICE_SERVICE";

    @Autowired
    private OrganizationRepository organizationRepository;

    @Autowired
    private BillingSubscriptionLogRepository billingSubscriptionLogRepository;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private FreeMarkerConfigurer freemarkerConfigurer;

    public int generateInvoice(LocalDate period, Organization organization) throws JRException, MessagingException, TemplateException, IOException {
        int savedData = 0;
        List<BillingSubscriptionLog> listBillingSubscriptionLog = billingSubscriptionLogRepository.findByMonthAndYearAndOrganizationAndInvoice(period.getMonthValue(), period.getYear(), organization, null);
        BigDecimal grandTotal = BigDecimal.ZERO;
        for (BillingSubscriptionLog billingSubscriptionLog : listBillingSubscriptionLog) {
            if (billingSubscriptionLog.getUsageSubtotalFlat() != null) {
                grandTotal = grandTotal.add(billingSubscriptionLog.getUsageSubtotalFlat());
            }
            if (billingSubscriptionLog.getUsageSubtotalTier1() != null) {
                grandTotal = grandTotal.add(billingSubscriptionLog.getUsageSubtotalTier1());
            }
            if (billingSubscriptionLog.getUsageSubtotalTier2() != null) {
                grandTotal = grandTotal.add(billingSubscriptionLog.getUsageSubtotalTier2());
            }
            if (billingSubscriptionLog.getUsageSubtotalTier3() != null) {
                grandTotal = grandTotal.add(billingSubscriptionLog.getUsageSubtotalTier3());
            }
            if (billingSubscriptionLog.getUsageSubtotalTier4() != null) {
                grandTotal = grandTotal.add(billingSubscriptionLog.getUsageSubtotalTier4());
            }
            if (billingSubscriptionLog.getUsageSubtotalTier5() != null) {
                grandTotal = grandTotal.add(billingSubscriptionLog.getUsageSubtotalTier5());
            }
        }

        if (listBillingSubscriptionLog != null && !listBillingSubscriptionLog.isEmpty()) {
            JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(listBillingSubscriptionLog, false);
            String toName = "";
            if (organization.getNotificationName().length() > 0) {
                toName = organization.getNotificationName();
            } else {
                toName = organization.getOwnerUsername();
            }

            Invoice invoice = generateNewInvoice(organization, period);
            if (invoice != null) {
                //add tax calculation
                log.info("Adding calculation tax");
                Double ppnPercent = Double.parseDouble(SystemPropertiesService.getPropertiesString("ppn.percent.value","10"))/100;
                Double pphPercent = Double.parseDouble(SystemPropertiesService.getPropertiesString("pph.percent.value","2"))/100;
                
                log.info("Ppn Percent --> " + ppnPercent);
                log.info("Pph Percent --> " + pphPercent);
                
                BigDecimal ppnAmount = grandTotal.multiply(new BigDecimal(ppnPercent));
                BigDecimal pphAmount = grandTotal.multiply(new BigDecimal(pphPercent));
                BigDecimal grandTotalAmount = grandTotal.subtract(pphAmount).add(ppnAmount);
                
                invoice.setTotalAmount(grandTotal);
                invoice.setPpnAmount(ppnAmount);
                invoice.setPphAmount(pphAmount);
                invoice.setGrandTotalAmount(grandTotalAmount);              
                //
                String folder = SystemPropertiesService.getPropertiesString("msdtiga.invoice.folder", "/home/tomcat/invoices/");
                String filename = folder + "api-invoice-" + invoice.getVirtualAccountNumber() + ".pdf";
                invoice.setFilePath(filename);
                invoiceRepository.save(invoice);
                savedData = savedData + 1;
                for (BillingSubscriptionLog billingSubscriptionLog : listBillingSubscriptionLog) {
                    billingSubscriptionLog.setInvoice(invoice);
                    billingSubscriptionLogRepository.save(billingSubscriptionLog);
                }

                HashMap<Object, Object> parameters = new HashMap<>();
                parameters.put("PERIOD", period.format(DateTimeFormatter.ofPattern("MM/yyyy")));
                parameters.put("PIC", toName);
                parameters.put("GRAND_TOTAL", grandTotal);
                parameters.put("VA_NUMBER", invoice.getVirtualAccountNumber());
                parameters.put("DUE_DATE", invoice.getDueDate().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
                parameters.put("INVOICE_NUMBER", ISOUtil.zeropad(invoice.getId(), 13));
                parameters.put("PPN_PERCENT",ppnPercent*100);
                parameters.put("PPH_PERCENT",pphPercent*100);
                parameters.put("PPN_AMOUNT",ppnAmount);
                parameters.put("PPH_AMOUNT",pphAmount);
                parameters.put("GRAND_TOTAL_AMOUNT",grandTotalAmount);
                
                

                String invoiceTemplateFile = SystemPropertiesService.getPropertiesString("msdtiga.invoice.template");

                JasperPrint jasperPrint = null;
                jasperPrint = generateJasperPrint(invoiceTemplateFile, parameters, beanColDataSource);
                exportToPdf(filename, jasperPrint);
                //sendMessageUsingFreemarkerTemplate(organization, filename);
            } else {
                log.info("Invoice already paid, not eligible to generate new Invoice belong to the organization of that period");
            }
        } else {
            log.info("No Data Found !!");
        }
        return savedData;
    }

    public int generateInvoice(LocalDate period) throws MessagingException, JRException, TemplateException, IOException {
        int savedData = 0;
        List<Organization> list = organizationRepository.findAll();
        for (Organization organization : list) {
            savedData = savedData + generateInvoice(period, organization);
        }
        return savedData;
    }

    public int invoiceNotification(LocalDate localDate) throws MessagingException, IOException, TemplateException {
        List<Invoice> list = invoiceRepository.findByPaidAndDeleted(false, false);
        int dueDateNotif = Integer.parseInt(SystemPropertiesService.getPropertiesString("msdtifa.invoice.duedate.notification"));
        String invoiceMailNotificationTemplate = SystemPropertiesService.getPropertiesString(
                "msdtiga.mail.template.invoice-duedate", "invoice-notification.ftl");
        Template freemarkerTemplate = freemarkerConfigurer.createConfiguration().getTemplate(invoiceMailNotificationTemplate);
        String sender = SystemPropertiesService.getPropertiesString("msdtiga.mail.sender.estatement");
        String subject = SystemPropertiesService.getPropertiesString("msdtiga.mail.subject");
        String notification = SystemPropertiesService.getPropertiesString("msdtiga.invoice.duedate.notification");
        int maxNotificationCounter = Integer.parseInt(SystemPropertiesService.getPropertiesString("msdtiga.invoice.duedate.notification.max"));

        int savedData = 0;
        if (!list.isEmpty()) {
            for (Invoice invoice : list) {
                if ((localDate.isEqual(invoice.getDueDate().minusDays(dueDateNotif)) 
                        || localDate.isAfter(invoice.getDueDate())) 
                        && invoice.getNotificationCounter() < maxNotificationCounter) {
                    
                    MimeMessage message = javaMailSender.createMimeMessage();
                    MimeMessageHelper helper = new MimeMessageHelper(message, true);
                    
                    //SimpleMailMessage message = new SimpleMailMessage();
                    helper.setTo(invoice.getOrganization().getNotificationEmail());
                    helper.setFrom(sender);
                    helper.setSubject(subject);
                    
                    Map<String, Object> templateModel = new HashMap<>();
                    templateModel.put("recipientName", invoice.getOrganization().getOwnerUsername());
                    templateModel.put("senderName", sender);
                    templateModel.put("dueDateCount", notification);
                    String htmlBody = FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerTemplate, templateModel);
                    helper.setText(htmlBody, true);
                    javaMailSender.send(message);
                    
                    invoice.setNotificationCounter(invoice.getNotificationCounter() + 1);
                    invoiceRepository.save(invoice);
                    savedData = savedData + 1;
                } else {
                    log.info(
                            "Invoice " + invoice.getFilePath() + " due date not reach... or exceed notification counter");
                }
            }
        } else {
            log.info("No Unpaid Invoice...");
        }
        return savedData;
    }

    public int invoiceNotification() throws MessagingException, IOException, TemplateException {
        return invoiceNotification(LocalDate.now());
    }

    private void exportToPdf(String filename, JasperPrint jasperPrint) throws JRException {
        JRPdfExporter exporter = new JRPdfExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(
                new SimpleOutputStreamExporterOutput(filename));

        SimplePdfReportConfiguration reportConfig
                = new SimplePdfReportConfiguration();
        reportConfig.setSizePageToContent(true);
        reportConfig.setForceLineBreakPolicy(false);

        SimplePdfExporterConfiguration exportConfig
                = new SimplePdfExporterConfiguration();
        exportConfig.setMetadataAuthor("mii");
        exportConfig.setEncrypted(false);
        exportConfig.setAllowedPermissionsHint("PRINTING");
        exporter.setConfiguration(reportConfig);
        exporter.setConfiguration(exportConfig);

        exporter.exportReport();
    }

    public JasperPrint generateJasperPrint(String path1, HashMap map, JRBeanCollectionDataSource beanDataSource) {
        try {
            return JasperFillManager.fillReport(path1, map, (JRDataSource) beanDataSource);
        } catch (JRException ex) {
            log.error(Utility.printException(ex));
            return null;
        }
    }

    private Invoice generateNewInvoice(Organization organization, LocalDate period) {
        Invoice invoice = new Invoice();
        StringBuilder virtualAccountNumber = new StringBuilder();
        String invoicePrefix = SystemPropertiesService.getPropertiesString("msdtiga.invoice.prefix");
        int dueDate = Integer.parseInt(SystemPropertiesService.getPropertiesString("msdtiga.invoice.duedate"));
        try {
            virtualAccountNumber.append(invoicePrefix)
                    .append(ISOUtil.zeropad(String.valueOf(organization.getOrganizationId()), 4))
                    .append(ISOUtil.zeropad(period.getMonthValue(), 2))
                    .append(period.getYear());
        } catch (ISOException e) {
            log.error(Utility.printException(e));
            virtualAccountNumber.append("88860000")
                    .append(period.getMonthValue())
                    .append(period.getYear());
        }

        List<Invoice> pastInvoices = invoiceRepository.findByMonthAndYearAndOrganization(
                period.getMonthValue(),
                period.getYear(),
                organization);
        boolean eligible = true;
        if (!pastInvoices.isEmpty()) {
            for (Invoice pastInvoice : pastInvoices) {
                if (pastInvoice.isPaid()) {
                    eligible = false;
                    break;
                }
            }
        }

        if (eligible) {
            int revision = 0;
            if (pastInvoices.isEmpty()) {
                revision = 1;
            } else {
                Invoice maxInvoice = invoiceRepository.findFirstByMonthAndYearAndOrganizationOrderByRevisionDesc(
                        period.getMonthValue(),
                        period.getYear(),
                        organization);
                maxInvoice.setDeleted(true);
                invoiceRepository.save(maxInvoice);
                revision = maxInvoice.getRevision() + 1;
            }
            virtualAccountNumber.append(ISOUtil.zeropad(revision, 2));
            invoice.setPaid(false);
            invoice.setVirtualAccountNumber(virtualAccountNumber.toString());
            invoice.setRevision(revision);
            invoice.setOrganization(organization);
            invoice.setMonth(period.getMonthValue());
            invoice.setYear(period.getYear());
            invoice.setGenerateTimestamp(LocalDateTime.now());
            invoice.setDueDate(LocalDate.now().plusDays(dueDate));
            invoice.setInvoiceDate(period.plusMonths(1));
            return invoice;
        } else {
            log.info("Not eligible to generate new invoice for '"+organization.getOrganizationName()+"'"
                    + " because the invoice for this period : '"+ period.getMonth() + "" + period.getYear() + "' are alreadi PAID");
            return null;
        }
    }
    
    public int sendInvoice(int year, int month) throws IOException, TemplateException, MessagingException {
        List<Organization> organizations = organizationRepository.findAll();
        int count = 0;
        for(Organization organization : organizations) {
            count = count + sendInvoice(organization, year, month);
        }
        return count;
    }
    
    public int sendInvoice(Organization organization, int year, int month) throws IOException, TemplateException, MessagingException {
        List<Invoice> list = invoiceRepository.findByYearAndMonthAndDeletedAndOrganization(year, month, false, organization);
        int count = 0;
        for(Invoice invoice : list) {
            String folder = SystemPropertiesService.getPropertiesString("msdtiga.invoice.folder", "/home/tomcat/invoices/");
            String filename = folder + "api-invoice-" + invoice.getVirtualAccountNumber() + ".pdf";
            sendMessageUsingFreemarkerTemplate(organization, filename);
            
            invoice.setSentCounter(invoice.getSentCounter() + 1);
            invoiceRepository.save(invoice);
            
            count++;
        }
        return count;
    }

    public void sendMessageUsingFreemarkerTemplate(Organization organization, String filename)
            throws IOException, TemplateException, MessagingException {
        String invoiceMailTemplate = SystemPropertiesService.getPropertiesString(
                "msdtiga.mail.template.invoice", "invoice.ftl");

        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        Template freemarkerTemplate = freemarkerConfigurer.createConfiguration().getTemplate(invoiceMailTemplate);

        String sender = SystemPropertiesService.getPropertiesString("msdtiga.mail.sender.estatement");
        String subject = SystemPropertiesService.getPropertiesString("msdtiga.mail.subject");
        if (organization.getNotificationEmail().length() > 0) {
            helper.setTo(organization.getNotificationEmail());
        } else {
            helper.setTo(organization.getOwnerEmail());
        }

        Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("recipientName", organization.getOwnerUsername());
        templateModel.put("senderName", sender);

        String htmlBody = FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerTemplate, templateModel);

        helper.setFrom(sender);
        helper.setSubject(subject);
        helper.setText(htmlBody, true);
        FileSystemResource file = new FileSystemResource(filename);
        helper.addAttachment(file.getFilename(), file);
        javaMailSender.send(message);
    }

}
