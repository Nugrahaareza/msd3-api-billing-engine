package com.mii.msdtiga.service;

import com.mii.msdtiga.entity.ApimConnection;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author vinch
 */
public class ApimConnectionService {
    
    public static List<ApimConnection> getAllApimConnections() {
        JSONArray jsonArray = new JSONArray(SystemPropertiesService.getPropertiesString("apim.credentials"));
        List<ApimConnection> conns = new ArrayList<>();
        for(int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            ApimConnection apimc = new ApimConnection();
            apimc.setUsername(jsonObject.getString("username"));
            apimc.setPassword(jsonObject.getString("password"));
            apimc.setCatalogName(jsonObject.getString("catalog"));
            apimc.setHostname(jsonObject.getString("hostname"));
            apimc.setOrgName(jsonObject.getString("orgprovider"));
            apimc.setPort(jsonObject.getInt("port"));
            apimc.setTimeout(jsonObject.getInt("timeout"));
            conns.add(apimc);
        }
        return conns;
    }
    
}
