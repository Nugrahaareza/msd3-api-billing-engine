package com.mii.msdtiga.service;

import com.mii.msdtiga.entity.PaymentTransaction;
import com.mii.msdtiga.repository.PaymentTransactionRepository;
import com.mii.msdtiga.repository.SystemPropertiesRepository;
import com.opencsv.CSVWriter;
import org.jpos.iso.ISOUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.jpos.iso.ISOException;

@Service
public class PaymentTransactionService {

    public static final String PAYMENT_TRX = "PAYMENT_TRX_SERVICE";

    @Autowired
    PaymentTransactionRepository paymentTransactionRepository;

    @Autowired
    SystemPropertiesRepository propertiesRepository;

    public List<PaymentTransaction> findBySystemTimestamp(LocalDate date) {
        List<PaymentTransaction> paymentTransactionList = paymentTransactionRepository.findSingleDay(date);
        return paymentTransactionList;
    }

    public List<PaymentTransaction> findBySystemTimestamp() {
        return findBySystemTimestamp(LocalDate.now());
    }

    public int generateCsv(LocalDate date) throws IOException, ISOException {
        String reconFolder = SystemPropertiesService.getPropertiesString("msdtiga.recon.folder");
        return writeData(reconFolder + "/recon-" + date.format(DateTimeFormatter.ofPattern("yyyyMMdd")) + ".csv", findBySystemTimestamp(date));
    }

    public int generateCsv() throws IOException, ISOException {
        return generateCsv(LocalDate.now());
    }

    private int writeData(String filePath, List<PaymentTransaction> list) throws IOException, ISOException {
        File file = new File(filePath);
        FileWriter outputfile = new FileWriter(file);
        CSVWriter writer = new CSVWriter(outputfile);
        writer.writeNext(new String[]{"reference_id", "authorization_response_id", "va_number", "name", "amount", "period", "due_date", "bank_reff", "local_datetime", "payment_date", "payment_time"});
        for (PaymentTransaction paymentTransaction : list) {
            String[] data = new String[11];
            data[0] = paymentTransaction.getPaymentReferenceId();
            data[1] = paymentTransaction.getPaymentAuthorizationResponseId();
            data[2] = paymentTransaction.getVirtualAccountNumber();
            data[3] = paymentTransaction.getOrganizationName();
            data[4] = String.valueOf(paymentTransaction.getPaymentAmount());
            data[5] = ISOUtil.zeropad(paymentTransaction.getPeriod(), 6);
            data[6] = paymentTransaction.getInvoiceDueDate().toString();
            data[7] = paymentTransaction.getPaymentBankReferenceNumber();
            data[8] = paymentTransaction.getSystemTimestamp().format(DateTimeFormatter.ofPattern("ddMMyyyyHHmmss"));
            data[9] = paymentTransaction.getPaymentDate().format(DateTimeFormatter.ofPattern("ddMMyyyy"));
            data[10] = paymentTransaction.getPaymentTime().format(DateTimeFormatter.ofPattern("HHmmss"));
            writer.writeNext(data);
        }
        writer.close();
        return list.size();
    }

}
