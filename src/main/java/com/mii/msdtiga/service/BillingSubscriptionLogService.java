package com.mii.msdtiga.service;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

import com.mii.msdtiga.entity.*;
import com.mii.msdtiga.repository.*;
import com.mii.msdtiga.util.DateUtil;
import com.mii.msdtiga.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class BillingSubscriptionLogService {

    private static final String SCHEME_TYPE_FLAT = "Flat";

    private static final int DEFAULT_USAGE = 0;

    private static final int MAX_TIER_LENGTH = 5;

    public static final String BILLING_SUBSCRIPTION_SERVICE = "BILLING_SUBSCRIPTION_SERVICE";

    static Logger log = LoggerFactory.getLogger(BillingSubscriptionLogService.class);

    @Autowired
    private BillingSubscriptionLogRepository billingSubscriptionLogRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private UsageRepository usageRepository;

    @Autowired
    private BillingSchemeChargeRepository billingSchemeChargeRepository;

    @Autowired
    private BillingSchemeChargeTemplateRepository billingSchemeTemplateChargeRepository;

    public void save(BillingSubscriptionLog bsl) {
        try {
            billingSubscriptionLogRepository.save(bsl);
        } catch (DataIntegrityViolationException ex) {
            log.error("Duplicate Entry, not saving to DB");
        }
    }

    public int calculateAndStoreBilling(Date date, Subscription subscription) throws NoSuchFieldException, IllegalAccessException, NullPointerException {
        BillingScheme billingScheme;
        LocalDate localDate = DateUtil.convertDateToLocalDate(date);

        BillingSubscriptionLog bsl = new BillingSubscriptionLog();
        //get usage per month by subscription id
        Integer usagePerMonth = usageRepository.countUsageByMonth(localDate.getYear(), localDate.getMonthValue(), subscription.getId());
        if (usagePerMonth == null) {
            System.out.println("Not found API usage for apps " + subscription.getAppName() + " id " + subscription.getId() + ", subscription id, " + subscription.getSubscriptionId() + " on month " + localDate.getYear() + "-" + localDate.getMonthValue());
            //continue;
            bsl.setUsageCount(0);
            //throw new NullPointerException();
            usagePerMonth = 0;
        }
        bsl.setUsageCount(usagePerMonth);

        //get billing scheme
        billingScheme = subscription.getBilllingScheme();
        //get list billing charge by scheme

        int savedData = 0;
        if (billingScheme != null) {
            log.info("Billing Schema is not null");
            BigDecimal totalCharges = BigDecimal.ZERO;
            if (billingScheme.getBillingSchemeTemplate() != null) {
                log.info("Using Template!!!");
                List<BillingSchemeTemplateCharge> listBillingCharge = new ArrayList<>();
                BillingSchemeTemplate billingSchemeTemplate = billingScheme.getBillingSchemeTemplate();
                listBillingCharge = billingSchemeTemplateChargeRepository.findByBillingSchemeTemplate(billingSchemeTemplate);
                if (billingSchemeTemplate.getBillingSchemeTemplateType().equals(SCHEME_TYPE_FLAT)) {
                    totalCharges = BigDecimal.valueOf(listBillingCharge.get(0).getBillingTemplateCharge() * usagePerMonth);
                    bsl.setUsageCountFlat(usagePerMonth);
                    bsl.setUsageChargeFlat(BigDecimal.valueOf(listBillingCharge.get(0).getBillingTemplateCharge()));
                    bsl.setUsageSubtotalFlat(totalCharges);
                } else {
                    int total = 0;
                    long tierSumCharges[] = new long[5];
                    int[] tierUsage = new int[]{0, 0, 0, 0, 0};
                    int[] tierMinCall = new int[5];
                    int[] tierMaxCall = new int[5];
                    long[] tierCharges = new long[]{0, 0, 0, 0, 0};
                    //check length tier
                    int tierLvl = listBillingCharge.size();
                    bsl.setTierLength(tierLvl);
                    for (int j = 0; j < tierLvl; j++) {
                        tierMinCall[j] = listBillingCharge.get(j).getMinCall();
                        tierMaxCall[j] = listBillingCharge.get(j).getMaxCall();
                        if (usagePerMonth > DEFAULT_USAGE) {
                            if (j == (tierLvl - 1)) {
                                tierSumCharges[j] = listBillingCharge.get(j).getBillingTemplateCharge() * usagePerMonth;
                                tierCharges[j] = listBillingCharge.get(j).getBillingTemplateCharge();
                                tierUsage[j] = usagePerMonth;
                            } else {
                                // remaining usage lower than limit tier
                                if (usagePerMonth < listBillingCharge.get(j).getMaxCall()) {
                                    tierSumCharges[j] = listBillingCharge.get(j).getBillingTemplateCharge() * usagePerMonth;
                                    tierCharges[j] = listBillingCharge.get(j).getBillingTemplateCharge();
                                    tierUsage[j] = usagePerMonth;
                                    usagePerMonth = DEFAULT_USAGE;
                                } else {
                                    tierSumCharges[j] = listBillingCharge.get(j).getBillingTemplateCharge() * listBillingCharge.get(j).getMaxCall();
                                    tierCharges[j] = listBillingCharge.get(j).getBillingTemplateCharge();
                                    tierUsage[j] = listBillingCharge.get(j).getMaxCall();
                                    usagePerMonth -= listBillingCharge.get(j).getMaxCall();
                                }
                            }
                        }
                        total += tierSumCharges[j];
                    }
                    totalCharges = BigDecimal.valueOf(total);
                    for (int k = 1; k <= MAX_TIER_LENGTH; k++) {

                        Field xFieldTierUsageCount = BillingSubscriptionLog.class.getDeclaredField("usageCountTier" + (k));
                        Field xFieldTierUsageCharge = BillingSubscriptionLog.class.getDeclaredField("usageChargeTier" + (k));
                        Field xFieldTierMinCall = BillingSubscriptionLog.class.getDeclaredField("minCallTier" + (k));
                        Field xFieldTierMaxCall = BillingSubscriptionLog.class.getDeclaredField("maxCallTier" + (k));
                        Field xFieldTierUsageSubtotal = BillingSubscriptionLog.class.getDeclaredField("usageSubtotalTier" + (k));
                        xFieldTierUsageCount.setAccessible(true);
                        xFieldTierUsageCharge.setAccessible(true);
                        xFieldTierMinCall.setAccessible(true);
                        xFieldTierMaxCall.setAccessible(true);
                        xFieldTierUsageSubtotal.setAccessible(true);

                        xFieldTierUsageCount.set(bsl, tierUsage[k - 1]);
                        xFieldTierUsageCharge.set(bsl, BigDecimal.valueOf(tierCharges[k - 1]));
                        xFieldTierMinCall.set(bsl, tierMinCall[k - 1]);
                        xFieldTierMaxCall.set(bsl, tierMaxCall[k - 1]);
                        xFieldTierUsageSubtotal.set(bsl, BigDecimal.valueOf(tierSumCharges[k - 1]));
                    }
                }
            } else {
                log.info("Using Billing Schema!!!");
                List<BillingSchemeCharge> listBillingCharge = billingSchemeChargeRepository.findByBillingScheme(billingScheme);
                //check billing scheme type
                if (listBillingCharge.size() > 0) {
                    if (billingScheme.getBillingSchemeType().equals(SCHEME_TYPE_FLAT)) {
                        //calculate charges for flat
                        totalCharges = BigDecimal.valueOf(listBillingCharge.get(0).getBillingCharge() * usagePerMonth);
                        bsl.setUsageCountFlat(usagePerMonth);
                        bsl.setUsageChargeFlat(BigDecimal.valueOf(listBillingCharge.get(0).getBillingCharge()));
                        bsl.setUsageSubtotalFlat(totalCharges);
                    } else {
                        int total = 0;
                        long tierSumCharges[] = new long[5];
                        int[] tierUsage = new int[]{0, 0, 0, 0, 0};
                        int[] tierMinCall = new int[5];
                        int[] tierMaxCall = new int[5];
                        long[] tierCharges = new long[]{0, 0, 0, 0, 0};
                        //check length tier
                        int tierLvl = listBillingCharge.size();
                        bsl.setTierLength(tierLvl);
                        for (int j = 0; j < tierLvl; j++) {
                            tierMinCall[j] = listBillingCharge.get(j).getMinCall();
                            tierMaxCall[j] = listBillingCharge.get(j).getMaxCall();
                            if (usagePerMonth > DEFAULT_USAGE) {
                                if (j == (tierLvl - 1)) {
                                    tierSumCharges[j] = listBillingCharge.get(j).getBillingCharge() * usagePerMonth;
                                    tierCharges[j] = listBillingCharge.get(j).getBillingCharge();
                                    tierUsage[j] = usagePerMonth;
                                } else {
                                    // remaining usage lower than limit tier
                                    if (usagePerMonth < listBillingCharge.get(j).getMaxCall()) {
                                        tierSumCharges[j] = listBillingCharge.get(j).getBillingCharge() * usagePerMonth;
                                        tierCharges[j] = listBillingCharge.get(j).getBillingCharge();
                                        tierUsage[j] = usagePerMonth;
                                        usagePerMonth = DEFAULT_USAGE;
                                    } else {
                                        tierSumCharges[j] = listBillingCharge.get(j).getBillingCharge() * listBillingCharge.get(j).getMaxCall();
                                        tierCharges[j] = listBillingCharge.get(j).getBillingCharge();
                                        tierUsage[j] = listBillingCharge.get(j).getMaxCall();
                                        usagePerMonth -= listBillingCharge.get(j).getMaxCall();
                                    }
                                }
                            }
                            total += tierSumCharges[j];
                        }
                        totalCharges = BigDecimal.valueOf(total);
                        for (int k = 1; k <= MAX_TIER_LENGTH; k++) {

                            Field xFieldTierUsageCount = BillingSubscriptionLog.class.getDeclaredField("usageCountTier" + (k));
                            Field xFieldTierUsageCharge = BillingSubscriptionLog.class.getDeclaredField("usageChargeTier" + (k));
                            Field xFieldTierMinCall = BillingSubscriptionLog.class.getDeclaredField("minCallTier" + (k));
                            Field xFieldTierMaxCall = BillingSubscriptionLog.class.getDeclaredField("maxCallTier" + (k));
                            Field xFieldTierUsageSubtotal = BillingSubscriptionLog.class.getDeclaredField("usageSubtotalTier" + (k));
                            xFieldTierUsageCount.setAccessible(true);
                            xFieldTierUsageCharge.setAccessible(true);
                            xFieldTierMinCall.setAccessible(true);
                            xFieldTierMaxCall.setAccessible(true);
                            xFieldTierUsageSubtotal.setAccessible(true);

                            xFieldTierUsageCount.set(bsl, tierUsage[k - 1]);
                            xFieldTierUsageCharge.set(bsl, BigDecimal.valueOf(tierCharges[k - 1]));
                            xFieldTierMinCall.set(bsl, tierMinCall[k - 1]);
                            xFieldTierMaxCall.set(bsl, tierMaxCall[k - 1]);
                            xFieldTierUsageSubtotal.set(bsl, BigDecimal.valueOf(tierSumCharges[k - 1]));
                        }
                    }
                } else {
                    log.info("Billing Schema not found !!");
                }
            }

            bsl.setSubscriptionChargeTotal(totalCharges);
            bsl.setSubscription(subscription);
            bsl.setMonth(localDate.getMonthValue());
            bsl.setYear(localDate.getYear());
            bsl.setAppName(subscription.getAppName());
            bsl.setOrg(subscription.getOrganization());
            bsl.setOrgName(subscription.getOrganization().getOrganizationName());
            bsl.setProductName(subscription.getProductName());
            bsl.setBillingSchemeName(billingScheme.getBillingSchemeName());
            bsl.setBillingSchemeType(billingScheme.getBillingSchemeType());
            //save to table
            save(bsl);
            savedData = savedData + 1;
        } else {
            log.info("Billing schema not set");
        }
        return savedData;
    }

    @Transactional
    public int calculateAndStoreBilling(Date date, Organization organization) throws NoSuchFieldException, IllegalAccessException {
        //LocalDate localDate = DateUtil.convertDateToLocalDate(date);
        List<Subscription> listSubscription = subscriptionRepository.findByOrganizationAndSubscriptionStatus(organization, SubscriptionStatus.SET);
        /*billingSubscriptionLogRepository.deleteByMonthAndYearAndOrganizationAndInvoice(
                localDate.getMonthValue(),
                localDate.getYear(),
                organization,
                null);*/
        int savedData = 0;
        boolean saved = false;
        for (Subscription subscription : listSubscription) {
            try {
                calculateAndStoreBilling(date, subscription);
                saved = true;
            } catch (NullPointerException e) {
                log.warn(Utility.printException(e));
                saved = false;
                continue;
            } finally {
                if (saved) {
                    savedData = savedData + 1;
                }
            }
        }
        return savedData;
    }

    @Transactional
    public int calculateAndStoreBilling(Date date) throws NoSuchFieldException, IllegalAccessException {
        LocalDate localDate = DateUtil.convertDateToLocalDate(date);
        List<Subscription> listSubscription = subscriptionRepository.findAll();
        billingSubscriptionLogRepository.deleteByMonthAndYearAndInvoice(
                localDate.getMonthValue(),
                localDate.getYear(),
                null);
        int savedData = 0;
        boolean saved = false;
        for (Subscription subscription : listSubscription) {
            try {
                calculateAndStoreBilling(date, subscription);
                saved = true;
            } catch (NullPointerException e) {
                saved = false;
                continue;
            } finally {
                if (saved) {
                    savedData = savedData + 1;
                }
            }
        }
        return savedData;
    }
}
