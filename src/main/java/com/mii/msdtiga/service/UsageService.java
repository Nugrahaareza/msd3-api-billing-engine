package com.mii.msdtiga.service;

import com.mii.msdtiga.entity.*;
import com.mii.msdtiga.repository.CallsRepository;
import com.mii.msdtiga.repository.OrganizationRepository;
import com.mii.msdtiga.repository.SubscriptionRepository;
import com.mii.msdtiga.repository.UsageRepository;
import com.mii.msdtiga.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.springframework.dao.DataIntegrityViolationException;

@Service
public class UsageService {

    @Autowired
    private UsageRepository usageRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private OrganizationRepository organizationRepository;

    @Autowired
    private CallsRepository callsRepository;

    public int countAndStoreUsage(LocalDate localDate) {
        return countAndStoreUsage(DateUtil.convertLocalDateToDate(localDate));
    }

    public int countAndStoreUsage(Date date) {
        LocalDate localDate = DateUtil.convertDateToLocalDate(date);
        List<Subscription> list = subscriptionRepository.findAll();
        int dataSaved = 0;
        if (!list.isEmpty()) {
            for (Subscription subscription : list) {
                Usage apiUsage = new Usage();
                UsageId usageId = new UsageId();

                String planId = subscription.getPlanId();
                String appId = subscription.getAppId();
                String devOrgId = subscription.getOrganization().getOrgApimId();

                String includedStatusCodes = SystemPropertiesService.getPropertiesString("data.usage.includedstatuscodes");
                String excludedResourceId = SystemPropertiesService.getPropertiesString("data.usage.excludedresource");
                int totalCount;
                if(includedStatusCodes.equals("*") && excludedResourceId.equals("*")) {
                    totalCount = callsRepository.countBySubscriptionPeriod(localDate, planId, appId, devOrgId);
                } else {
                    if(includedStatusCodes.equals("*")) {
                        List<String> excludedRes = Arrays.asList(excludedResourceId.split(","));
                        totalCount = callsRepository.countBySubscriptionPeriodAndResourceIdNotIn(localDate, planId, appId, devOrgId, excludedRes);
                    } else if(excludedResourceId.equals("*")) {
                        List<String> includedSc = Arrays.asList(includedStatusCodes.split(","));
                        totalCount = callsRepository.countBySubscriptionPeriodAndStatusCode(localDate, planId, appId, devOrgId, includedSc);
                    } else {
                        List<String> includedSc = Arrays.asList(includedStatusCodes.split(","));
                        List<String> excludedRes = Arrays.asList(excludedResourceId.split(","));
                        totalCount = callsRepository.countBySubscriptionPeriodAndStatusCodeAndResourceIdNotIn(localDate, planId, appId, devOrgId, includedSc, excludedRes);
                    }
                }
                apiUsage.setUsageCount(totalCount);

                Organization devOrganizationObject = organizationRepository.findByOrgApimId(devOrgId);
                usageId.setSubscription(subscriptionRepository.findByAppIdAndPlanIdAndOrganization(appId, planId, devOrganizationObject).get(0));
                usageId.setPeriod(DateUtil.convertDateToLocalDate(date));
                apiUsage.setUsageId(usageId);

                boolean saved = false;
                try {
                    usageRepository.save(apiUsage);
                    saved = true;
                } catch (DataIntegrityViolationException ex) {
                    System.out.println(ex.getMessage());
                    saved = false;
                } finally {
                    if (saved) {
                        dataSaved = dataSaved + 1;
                    }
                    System.out.println("Current saved data : " + dataSaved);
                }
            }
            //callsRepository.deleteDataByDate(localDate);
        }
        return dataSaved;
    }

}
