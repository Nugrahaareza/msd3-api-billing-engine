package com.mii.msdtiga.service;

import com.mii.msdtiga.APIRequestFactory;
import com.mii.msdtiga.entity.*;
import com.mii.msdtiga.repository.OrganizationRepository;
import com.mii.msdtiga.repository.SubscriptionRepository;
import com.mii.msdtiga.util.HttpMessage;
import com.mii.msdtiga.util.HttpUtil;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.core.task.TaskExecutor;
import org.springframework.mail.SimpleMailMessage;

@Service
public class SubscriptionService {

    public static final String SUBSCRIPTION_SERVICE = "SUBSCRIPTION_SERVICE";

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private OrganizationRepository organizationRepository;

    @Autowired
    private JavaMailSender javaMailSender;

    public int triggerNotification() throws MessagingException {
        String simasPICMail = SystemPropertiesService.getPropertiesString("msdtiga.mail.pic");
        List<Subscription> subs = subscriptionRepository.findBySubscriptionStatusAndBillingScheme(SubscriptionStatus.NOT_SET, null);
        int savedData = 0;
        
        String sender = SystemPropertiesService.getPropertiesString("msdtiga.mail.sender.system");
        
        if (!subs.isEmpty()) {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setSubject("Notification Email from API Billing System");
            message.setFrom(sender);
            message.setTo(simasPICMail);
            String content = "";
            for (Subscription sub : subs) {
                content = content + "\n- Apps Name : " + sub.getAppName() + ","
                        + " Orgs Name : " + sub.getOrganization().getOrganizationName() + ","
                        + " API Product : " + sub.getProductName() + "\n";
                savedData = savedData + 1;
            }
            message.setText("Dear System Admin,"
                    + content
                    + "Harap segera setting skema billing untuk data subscription di atas.");
            javaMailSender.send(message);
        } else {
            System.out.println("No Pending Task");
        }
        return savedData;
    }
    
    @Autowired
    private TaskExecutor threadExecutor;

    private void executeAsynchronously(ApimConnection apimConnection) {
        threadExecutor.execute(() -> {
            try {
                fetchAndStore(apimConnection);
            } catch (IOException | URISyntaxException ex) {
                Logger.getLogger(CallsService.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    public void fetchAndStoreMultipleData() {
        ApimConnectionService.getAllApimConnections().forEach(spimConnection -> {
            executeAsynchronously(spimConnection);
        });
    }

    public int fetchAndStore(ApimConnection apimConnection) throws IOException, URISyntaxException {
        String hostname = apimConnection.getHostname();
        int port = apimConnection.getPort();
        int timeout = apimConnection.getTimeout();
        String catalogName = apimConnection.getCatalogName();
        String orgproviderName = apimConnection.getOrgName();
        String username = apimConnection.getUsername();
        String password = apimConnection.getPassword();

        int savedData = 0;
        HttpMessage msg = APIRequestFactory.invokeAPI(hostname, port, HttpUtil.HTTP_METHOD_GET,
                "v1/orgs/" + orgproviderName + "/environments/" + catalogName + "/subscriptions",
                username,
                password,
                timeout,
                null);
        JSONArray array = new JSONArray(msg.getBody());
        for (int i = 0; i < array.length(); i++) {
            JSONObject subscriptionObject = array.getJSONObject(i);

            JSONObject consumerOrgsObject = subscriptionObject.getJSONObject("consumerOrg");
            Organization org = organizationRepository.findByOrgApimId(consumerOrgsObject.getString("id"));

            JSONObject applicationObject = subscriptionObject.getJSONObject("application");
            JSONObject planObject = subscriptionObject.getJSONObject("plan");
            JSONObject productObject = subscriptionObject.getJSONObject("product");

            Subscription subscription = new Subscription();
            subscription.setOrganization(org);
            subscription.setAppId(applicationObject.getString("appId"));
            subscription.setAppName(applicationObject.getString("appName"));
            subscription.setPlanId(planObject.getString("id"));
            subscription.setPlanName(planObject.getString("name"));
            subscription.setProductName(productObject.getString("name"));
            subscription.setSubscriptionId(subscriptionObject.getString("id"));
            subscription.setBillingStatus(SubscriptionStatus.NOT_SET);
            subscription.setBillingScheme(null);
            
            boolean saved = false;
            try {
                subscriptionRepository.save(subscription);
                saved = true;
            } catch (DataIntegrityViolationException ex) {
                System.out.println("Duplicate Entry, not saving to DB");
                saved = false;
            } finally {
                if (saved) {
                    savedData = savedData + 1;
                }
                System.out.println("Current saved data = " + savedData);
            }
        }
        return savedData;
    }

}
