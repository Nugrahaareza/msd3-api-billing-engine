package com.mii.msdtiga.service;

import com.mii.msdtiga.entity.JobTrail;
import com.mii.msdtiga.repository.JobTrailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JobTrailService {

    @Autowired
    private JobTrailRepository jobTrailRepository;

    public void save(JobTrail jobTrail) {
        jobTrailRepository.saveAndFlush(jobTrail);
    }

}
