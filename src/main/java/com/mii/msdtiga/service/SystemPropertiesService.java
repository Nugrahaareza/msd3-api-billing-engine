package com.mii.msdtiga.service;

import com.mii.msdtiga.entity.SystemProperties;
import com.mii.msdtiga.repository.SystemPropertiesRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author vinch
 */
@Service
public class SystemPropertiesService {
    
    public static HashMap<String, String> propertiesMap = new HashMap<>();
    
    public static final Logger log = LoggerFactory.getLogger(SystemProperties.class);
    
    @Autowired
    private SystemPropertiesRepository repo;
    
    public String getProperty(String name) {
        return repo.findByPropertyName(name).getPropertyValue();
    }
    
    public static String getPropertiesString(String propName, String defaultValue) {
        return (String) propertiesMap.getOrDefault(propName, defaultValue);
    }
    
    public static String getPropertiesString(String propName) {
        return (String) propertiesMap.get(propName);
    }
    
    public static Integer getPropertiesInt(String propName, Integer defaultValue) {
        return Integer.parseInt(propertiesMap.getOrDefault(propName, String.valueOf(defaultValue)));
    }

    public static Integer getPropertiesInt(String propName) {
        return Integer.parseInt((String) propertiesMap.get(propName));
    }
    
    public int reloadParameter() {
        propertiesMap.clear();
        log.info("Executing reload parameter...");
        int i = 0;
        for(SystemProperties systemProperties : repo.findAll()) {
            propertiesMap.put(systemProperties.getPropertyName(), systemProperties.getPropertyValue());
            log.info(systemProperties.getPropertyName() + "=" + systemProperties.getPropertyValue());
            i++;
        }
        return i;
    }
    
    @PostConstruct
    public void constructInitialData() {
        if(repo.count() == 0) {
            List<SystemProperties> systemProperties = new ArrayList<>();
            
            SystemProperties sp = new SystemProperties();
            sp.setPropertyName("apim.hostname");
            sp.setPropertyValue("apimgmtdev.banksinarmas.com");
            sp.setLastModified(LocalDateTime.now());
            systemProperties.add(sp);
            
            SystemProperties sp1 = new SystemProperties();
            sp1.setPropertyName("apim.port");
            sp1.setPropertyValue("443");
            sp1.setLastModified(LocalDateTime.now());
            systemProperties.add(sp1);
            
            SystemProperties sp2 = new SystemProperties();
            sp2.setPropertyName("apim.username");
            sp2.setPropertyValue("admin.sandbox@banksinarmas.com");
            sp2.setLastModified(LocalDateTime.now());
            systemProperties.add(sp2);
            
            SystemProperties sp3 = new SystemProperties();
            sp3.setPropertyName("apim.password");
            sp3.setPropertyValue("s4ndb0xS1m4s!");
            sp3.setLastModified(LocalDateTime.now());
            systemProperties.add(sp3);
            
            SystemProperties sp4 = new SystemProperties();
            sp4.setPropertyName("apim.catalog.name");
            sp4.setPropertyValue("service");
            sp4.setLastModified(LocalDateTime.now());
            systemProperties.add(sp4);
            
            SystemProperties sp5 = new SystemProperties();
            sp5.setPropertyName("apim.orgprovider.name");
            sp5.setPropertyValue("sandbox");
            sp5.setLastModified(LocalDateTime.now());
            systemProperties.add(sp5);
            
            SystemProperties sp6 = new SystemProperties();
            sp6.setPropertyName("apim.basepath");
            sp6.setPropertyValue("/v1/orgs");
            sp6.setLastModified(LocalDateTime.now());
            systemProperties.add(sp6);
            
            SystemProperties sp7 = new SystemProperties();
            sp7.setPropertyName("apim.request.timeout");
            sp7.setPropertyValue("60000");
            sp7.setLastModified(LocalDateTime.now());
            systemProperties.add(sp7);
            
            SystemProperties sp8 = new SystemProperties();
            sp8.setPropertyName("msdtiga.mail.pic");
            sp8.setPropertyValue("erwin.santoso@mii.co.id");
            sp8.setLastModified(LocalDateTime.now());
            systemProperties.add(sp8);
            
            SystemProperties sp9 = new SystemProperties();
            sp9.setPropertyName("msdtiga.invoice.duedate");
            sp9.setPropertyValue("10");
            sp9.setLastModified(LocalDateTime.now());
            systemProperties.add(sp9);
            
            SystemProperties sp10 = new SystemProperties();
            sp10.setPropertyName("msdtiga.invoice.duedate.notification");
            sp10.setPropertyValue("2");
            sp10.setLastModified(LocalDateTime.now());
            systemProperties.add(sp10);
            
            SystemProperties sp11 = new SystemProperties();
            sp11.setPropertyName("job.cron.duedate.notif");
            sp11.setPropertyValue("0 0 10 ? * * *");
            sp11.setLastModified(LocalDateTime.now());
            systemProperties.add(sp11);
            
            SystemProperties sp12 = new SystemProperties();
            sp12.setPropertyName("job.cron.recon.file");
            sp12.setPropertyValue("0 0 5 ? * * *");
            sp12.setLastModified(LocalDateTime.now());
            systemProperties.add(sp12);
            
            SystemProperties sp13 = new SystemProperties();
            sp13.setPropertyName("job.cron.billing.notif");
            sp13.setPropertyValue("0 0 9 ? * * *");
            sp13.setLastModified(LocalDateTime.now());
            systemProperties.add(sp13);
            
            SystemProperties sp14 = new SystemProperties();
            sp14.setPropertyName("job.cron.invoice");
            sp14.setPropertyValue("0 0 10 2 * ? *");
            sp14.setLastModified(LocalDateTime.now());
            systemProperties.add(sp14);
            
            SystemProperties sp15 = new SystemProperties();
            sp15.setPropertyName("job.cron.billing");
            sp15.setPropertyValue("0 0 1 1 * ? *");
            sp15.setLastModified(LocalDateTime.now());
            systemProperties.add(sp15);
            
            SystemProperties sp16 = new SystemProperties();
            sp16.setPropertyName("job.cron.usage");
            sp16.setPropertyValue("0 0 3 ? * * *");
            sp16.setLastModified(LocalDateTime.now());
            systemProperties.add(sp16);
            
            SystemProperties sp17 = new SystemProperties();
            sp17.setPropertyName("job.cron.staging");
            sp17.setPropertyValue("0 0 1 ? * * *");
            sp17.setLastModified(LocalDateTime.now());
            systemProperties.add(sp17);
            
            SystemProperties sp18 = new SystemProperties();
            sp18.setPropertyName("job.cron.organizations");
            sp18.setPropertyValue("0 0 0/1 1/1 * ? *");
            sp18.setLastModified(LocalDateTime.now());
            systemProperties.add(sp18);
            
            SystemProperties sp19 = new SystemProperties();
            sp19.setPropertyName("job.cron.subscriptions");
            sp19.setPropertyValue("0 0 0/1 1/1 * ? *");
            sp19.setLastModified(LocalDateTime.now());
            systemProperties.add(sp19);
            
            SystemProperties sp20 = new SystemProperties();
            sp20.setPropertyName("msdtiga.mail.sender");
            sp20.setPropertyValue("api.sandbox-noreply@banksinarmas.com");
            sp20.setLastModified(LocalDateTime.now());
            systemProperties.add(sp20);
            
            SystemProperties sp21 = new SystemProperties();
            sp21.setPropertyName("msdtiga.mail.subject");
            sp21.setPropertyValue("Invoice API Sinarmas");
            sp21.setLastModified(LocalDateTime.now());
            systemProperties.add(sp21);
            
            SystemProperties sp22 = new SystemProperties();
            sp22.setPropertyName("msdtiga.mail.template.invoice");
            sp22.setPropertyValue("invoice.ftl");
            sp22.setLastModified(LocalDateTime.now());
            systemProperties.add(sp22);
            
            SystemProperties sp23 = new SystemProperties();
            sp23.setPropertyName("msdtiga.mail.template.invoice-duedate");
            sp23.setPropertyValue("invoice-duedate.ftl");
            sp23.setLastModified(LocalDateTime.now());
            systemProperties.add(sp23);
            
            SystemProperties sp24 = new SystemProperties();
            sp24.setPropertyName("msdtiga.invoice.template");
            sp24.setPropertyValue("/usr/local/tomcat9/webapps/api-billing-engine/WEB-INF/classes/invoice.jasper");
            sp24.setLastModified(LocalDateTime.now());
            systemProperties.add(sp24);
            
            SystemProperties sp25 = new SystemProperties();
            sp25.setPropertyName("msdtiga.mail.sendername");
            sp25.setPropertyValue("API Sandbox Admin");
            sp25.setLastModified(LocalDateTime.now());
            systemProperties.add(sp25);
            
            SystemProperties sp26 = new SystemProperties();
            sp26.setPropertyName("msdtiga.invoice.folder");
            sp26.setPropertyValue("/home/tomcat/invoices/");
            sp26.setLastModified(LocalDateTime.now());
            sp26.setPropertyDescription("Alamat email pengirim informasi invoice");
            systemProperties.add(sp26);
            
            SystemProperties sp27 = new SystemProperties();
            sp27.setPropertyName("msdtiga.mail.sender.estatement");
            sp27.setPropertyValue("estatement@banksinarmas.co.id");
            sp27.setLastModified(LocalDateTime.now());
            sp27.setPropertyDescription("Alamat email pengirim informasi invoice");
            systemProperties.add(sp27);
            
            SystemProperties sp28 = new SystemProperties();
            sp28.setPropertyName("data.usage.includedstatuscodes");
            sp28.setPropertyValue("*");
            sp28.setPropertyDescription("Parameter untuk include HTTP status code dari perhitungan API Usage (* for include all)");
            sp28.setLastModified(LocalDateTime.now());
            systemProperties.add(sp28);
            
            repo.saveAll(systemProperties);
            
        }
        
        reloadParameter();
        
    }
    
}
