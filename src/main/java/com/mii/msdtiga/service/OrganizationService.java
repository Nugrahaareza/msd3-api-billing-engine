package com.mii.msdtiga.service;

import com.mii.msdtiga.APIRequestFactory;
import com.mii.msdtiga.entity.ApimConnection;
import com.mii.msdtiga.entity.Organization;
import com.mii.msdtiga.repository.OrganizationRepository;
import com.mii.msdtiga.util.HttpMessage;
import com.mii.msdtiga.util.HttpUtil;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.core.task.TaskExecutor;

@Service
public class OrganizationService {

    public static final String ORGANIZATION_SERVICE = "ORGANIZATION_SERVICE";

    @Autowired
    private OrganizationRepository organizationRepository;

    @Autowired
    private TaskExecutor threadExecutor;

    private void executeAsynchronously(ApimConnection apimConnection) {
        threadExecutor.execute(() -> {
            try {
                fetchAndStore(apimConnection);
            } catch (IOException | URISyntaxException ex) {
                Logger.getLogger(CallsService.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    public void fetchAndStoreMultipleData() {
        ApimConnectionService.getAllApimConnections().forEach(spimConnection -> {
            executeAsynchronously(spimConnection);
        });
    }

    public Organization findByOrgApimId(String orgApimId) {
        return organizationRepository.findByOrgApimId(orgApimId);
    }

    public int fetchAndStore(ApimConnection apimConnection) throws IOException, URISyntaxException {
        String hostname = apimConnection.getHostname();
        int port = apimConnection.getPort();
        int timeout = apimConnection.getTimeout();
        String catalogName = apimConnection.getCatalogName();
        String orgproviderName = apimConnection.getOrgName();
        String username = apimConnection.getUsername();
        String password = apimConnection.getPassword();

        String requestUrl = "v1/orgs/" + orgproviderName + "/environments/" + catalogName + "/consumerOrgs";

        HttpMessage msg = APIRequestFactory.invokeAPI(hostname, port, HttpUtil.HTTP_METHOD_GET, requestUrl, username, password, timeout, null);
        int savedData = 0;
        if (msg.getStatusCode() == 200) {
            JSONArray array = new JSONArray(msg.getBody());
            savedData = 0;
            for (int i = 0; i < array.length(); i++) {
                JSONObject organizationObj = array.getJSONObject(i);
                JSONObject ownerObj = organizationObj.getJSONObject("owner");
                Organization org = new Organization();
                org.setNotificationEmail(ownerObj.getString("email"));
                org.setNotificationName(ownerObj.getString("firstName") + " " + ownerObj.getString("lastName"));
                org.setOrganizationName(organizationObj.getString("displayName"));
                org.setOrganizationStatus(organizationObj.getString("status"));
                org.setOrganizationUrl(organizationObj.getString("url"));
                org.setOrgApimId(organizationObj.getString("id"));
                org.setOwnerEmail(ownerObj.getString("email"));
                org.setOwnerUsername(ownerObj.getString("firstName") + " " + ownerObj.getString("lastName"));
                org.setCatalogName(catalogName);
                org.setProviderOrgName(orgproviderName);
                boolean saved = false;
                try {
                    organizationRepository.save(org);
                    saved = true;
                } catch (DataIntegrityViolationException ex) {
                    System.out.println(ex.getMessage());
                    saved = false;
                } finally {
                    if (saved) {
                        savedData = savedData + 1;
                    }
                    System.out.println("Current saved data : " + savedData);
                }
            }
        } else {
            System.out.println("request url : " + requestUrl + " return http status code : " + msg.getStatusCode());
            System.out.println("username : " + username);
            System.out.println("password : " + password);
        }
        return savedData;
    }

}
