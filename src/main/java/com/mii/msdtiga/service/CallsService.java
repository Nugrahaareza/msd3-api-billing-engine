package com.mii.msdtiga.service;

import com.google.gson.*;
import com.mii.msdtiga.APIRequestFactory;
import com.mii.msdtiga.entity.ApimConnection;
import com.mii.msdtiga.entity.Calls;
import com.mii.msdtiga.entity.Organization;
import com.mii.msdtiga.entity.Subscription;
import com.mii.msdtiga.entity.Usage;
import com.mii.msdtiga.entity.UsageId;
import com.mii.msdtiga.repository.CallsRepository;
import com.mii.msdtiga.repository.OrganizationRepository;
import com.mii.msdtiga.repository.SubscriptionRepository;
import com.mii.msdtiga.repository.UsageRepository;
import com.mii.msdtiga.util.DateUtil;
import com.mii.msdtiga.util.HttpMessage;
import com.mii.msdtiga.util.HttpUtil;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import static java.time.temporal.ChronoUnit.DAYS;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.slf4j.LoggerFactory;

@Service
public class CallsService {

    public static String PAGING_LIMIT = "10000";

    static org.slf4j.Logger log = LoggerFactory.getLogger(CallsService.class);

    @Autowired
    private CallsRepository callsRepository;

    @Autowired
    private UsageRepository usageRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private OrganizationRepository organizationRepository;

    public int fetchAndStoreData(ApimConnection apimConnection, String startDate, String endDate) throws IOException, URISyntaxException {
        List<NameValuePair> params = new ArrayList<>();
        NameValuePair limitNvp = new BasicNameValuePair("limit", PAGING_LIMIT);
        params.add(new BasicNameValuePair("after", startDate));
        params.add(new BasicNameValuePair("before", endDate));
        params.add(limitNvp);
        HttpMessage msg = APIRequestFactory.invokeAPI(apimConnection.getHostname(), apimConnection.getPort(), HttpUtil.HTTP_METHOD_GET,
                "v1/orgs/" + apimConnection.getOrgName() + "/environments/" + apimConnection.getCatalogName() + "/events",
                apimConnection.getUsername(),
                apimConnection.getPassword(),
                apimConnection.getTimeout(),
                params);
        int savedData = 0;
        if (msg.getStatusCode() == 200) {
            JSONObject json = new JSONObject(msg.getBody());
            JSONArray array = json.getJSONArray("calls");
            if (array.length() > 0) {
                int totalCalls = json.getInt("totalCalls");
                String next = json.getString("next");
                if (totalCalls > Integer.parseInt(PAGING_LIMIT)) {
                    while (array.length() > 0) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject obj = array.getJSONObject(i);
                            Gson gson = new GsonBuilder().registerTypeAdapter(LocalDateTime.class, new JsonDeserializer<LocalDateTime>() {
                                @Override
                                public LocalDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                                    return LocalDateTime.parse(json.getAsString(), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX"));
                                }
                            }).create();
                            Calls c = gson.fromJson(obj.toString(), Calls.class);
                            boolean saved = false;
                            try {
                                if (!c.getAppName().equals("N/A")) {
                                    callsRepository.save(c);
                                    saved = true;
                                }
                            } catch (DataIntegrityViolationException ex) {
                                System.out.println("Duplicate Entry, not saving to DB");
                                saved = false;
                            } finally {
                                if (saved) {
                                    savedData = savedData + 1;
                                }
                            }
                        }
                        params.remove(limitNvp);
                        params.add(new BasicNameValuePair("next", next));
                        APIRequestFactory req2 = new APIRequestFactory();
                        HttpMessage returnMsg = req2.invokeAPI(apimConnection.getHostname(), apimConnection.getPort(), HttpUtil.HTTP_METHOD_GET,
                                "v1/orgs/" + apimConnection.getOrgName() + "/environments/" + apimConnection.getCatalogName() + "/events",
                                apimConnection.getUsername(),
                                apimConnection.getPassword(),
                                apimConnection.getTimeout(),
                                params);
                        json = new JSONObject(returnMsg.getBody());
                        array = json.getJSONArray("calls");
                    }
                } else {
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        Gson gson = new GsonBuilder().registerTypeAdapter(LocalDateTime.class, new JsonDeserializer<LocalDateTime>() {
                            @Override
                            public LocalDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                                return LocalDateTime.parse(json.getAsString(), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX"));
                            }
                        }).create();
                        Calls c = gson.fromJson(obj.toString(), Calls.class);
                        boolean saved = false;
                        try {
                            if (!c.getAppName().equals("N/A")) {
                                callsRepository.save(c);
                                saved = true;
                            }
                        } catch (DataIntegrityViolationException ex) {
                            log.warn("Duplicate entry will not saved cause by " + ex.getMessage());
                            saved = false;
                        } finally {
                            if (saved) {
                                savedData = savedData + 1;
                            }
                        }
                    }
                }
            }
        } else {
            log.info("Request for Organization Name : " +apimConnection.getOrgName()
                    + ",Catalog Name : " +apimConnection.getCatalogName()
                    + " with username : " +apimConnection.getUsername()
                    + " result failed : " + msg.getStatusCode() + " : '" + msg.getBody() + "'");
        }
        return savedData;
    }

    public int fetchAndStoreData(ApimConnection apimConnection, LocalDate startDate, LocalDate endDate) throws IOException, URISyntaxException {
        Date start = DateUtil.convertLocalDateToDate(startDate);
        Date end = DateUtil.convertLocalDateToDate(endDate);
        return fetchAndStoreData(apimConnection, start, end);
    }

    public int fetchAndStoreData(ApimConnection apimConnection, Date startDate, Date endDate) throws IOException, URISyntaxException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        String sDate = dateFormat.format(startDate);
        String eDate = dateFormat.format(endDate);

        return fetchAndStoreData(apimConnection, sDate, eDate);
    }

    public void deleteByDateBetween(String startDate, String endDate) {
        LocalDate sDate = LocalDate.parse(startDate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        LocalDate eDate = LocalDate.parse(endDate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        callsRepository.deleteDataByDate(sDate, eDate);
    }

    public void deleteByDateBetween(LocalDate startDate, LocalDate endDate) {
        callsRepository.deleteDataByDate(startDate, endDate);
    }

    public void deleteCallsByDateBefore() {
        int dataRetentionPeriod = SystemPropertiesService.getPropertiesInt("period.data.retention");
        LocalDate now = LocalDate.now();
        LocalDate deleteStartDate = now.minus(dataRetentionPeriod, DAYS);
        callsRepository.deleteByTimestampBefore(deleteStartDate.atStartOfDay());
    }
    
    public int getCallsByDateBefore() {
        int dataRetentionPeriod = SystemPropertiesService.getPropertiesInt("period.data.retention");
        LocalDate now = LocalDate.now();
        LocalDate deleteStartDate = now.minus(dataRetentionPeriod, DAYS);
        return callsRepository.countByTimestampBefore(deleteStartDate.atStartOfDay());
    }

    public void deleteUsageByDateBefore() {
        int dataRetentionPeriod = SystemPropertiesService.getPropertiesInt("period.data.retention");
        LocalDate now = LocalDate.now();
        LocalDate deleteStartDate = now.minus(dataRetentionPeriod, DAYS);
        usageRepository.deleteByUsageIdPeriodBefore(deleteStartDate);
    }

    public int countAndStoreUsage(LocalDate localDate) {
        return countAndStoreUsage(DateUtil.convertLocalDateToDate(localDate));
    }

    public int countAndStoreUsage(Date date) {
        LocalDate localDate = DateUtil.convertDateToLocalDate(date);
        List<Subscription> list = subscriptionRepository.findAll();
        int dataSaved = 0;
        if (!list.isEmpty()) {
            for (Subscription subscription : list) {
                Usage apiUsage = new Usage();
                UsageId usageId = new UsageId();

                String planId = subscription.getPlanId();
                String appId = subscription.getAppId();
                String devOrgId = subscription.getOrganization().getOrgApimId();

                String includedStatusCodes = SystemPropertiesService.getPropertiesString("data.usage.includedstatuscodes");
                String excludedResourceId = SystemPropertiesService.getPropertiesString("data.usage.excludedresource");
                int totalCount;
                if (includedStatusCodes.equals("*") && excludedResourceId.equals("*")) {
                    totalCount = callsRepository.countBySubscriptionPeriod(localDate, planId, appId, devOrgId);
                } else {
                    if (includedStatusCodes.equals("*")) {
                        List<String> excludedRes = Arrays.asList(excludedResourceId.split(","));
                        totalCount = callsRepository.countBySubscriptionPeriodAndResourceIdNotIn(localDate, planId, appId, devOrgId, excludedRes);
                    } else if (excludedResourceId.equals("*")) {
                        List<String> includedSc = Arrays.asList(includedStatusCodes.split(","));
                        totalCount = callsRepository.countBySubscriptionPeriodAndStatusCode(localDate, planId, appId, devOrgId, includedSc);
                    } else {
                        List<String> includedSc = Arrays.asList(includedStatusCodes.split(","));
                        List<String> excludedRes = Arrays.asList(excludedResourceId.split(","));
                        totalCount = callsRepository.countBySubscriptionPeriodAndStatusCodeAndResourceIdNotIn(localDate, planId, appId, devOrgId, includedSc, excludedRes);
                    }
                }
                apiUsage.setUsageCount(totalCount);

                Organization devOrganizationObject = organizationRepository.findByOrgApimId(devOrgId);
                usageId.setSubscription(subscriptionRepository.findByAppIdAndPlanIdAndOrganization(appId, planId, devOrganizationObject).get(0));
                usageId.setPeriod(DateUtil.convertDateToLocalDate(date));
                apiUsage.setUsageId(usageId);

                boolean saved = false;
                try {
                    usageRepository.save(apiUsage);
                    saved = true;
                } catch (DataIntegrityViolationException ex) {
                    log.warn("Duplicate entry will not saved cause by " + ex.getMessage());
                    saved = false;
                } finally {
                    if (saved) {
                        dataSaved = dataSaved + 1;
                    }
                }
            }
        }
        return dataSaved;
    }

}
