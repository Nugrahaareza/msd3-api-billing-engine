package com.mii.msdtiga.service;

import com.mii.msdtiga.repository.AuthorityPageRepository;
import com.mii.msdtiga.repository.PagesRepository;
import com.mii.msdtiga.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class AuthorityPageService {

    @Autowired
    private AuthorityPageRepository authorityPageRepository;

    @Autowired
    private PagesRepository pagesRepository;

    @Autowired
    private RoleRepository roleRepository;

}
