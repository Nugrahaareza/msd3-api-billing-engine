package com.mii.msdtiga.controller;

import com.mii.msdtiga.util.JobUtil;
import com.mii.msdtiga.util.Utility;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOUtil;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/rest/trigger")
public class OnDemandJobController {

    private static String JOB_ORIGIN_ON_DEMAND = "ON_DEMAND_JOB_EXECUTOR";

    @Autowired
    @Qualifier("organizationDataSyncJobDetail")
    private JobDetail organizationDataSyncJobDetail;

    @Autowired
    @Qualifier("subscriptionDataSyncJobDetail")
    private JobDetail subscriptionDataSyncJobDetail;

    @Autowired
    @Qualifier("apiUsageJobDetail")
    private JobDetail apiUsageJobDetail;

    @Autowired
    @Qualifier("housekeepingJobDetail")
    private JobDetail housekeepingJobDetail;

    @Autowired
    @Qualifier("invoiceGeneratorJobDetail")
    private JobDetail invoiceGeneratorJobDetail;

    @Autowired
    @Qualifier("billingNotificationJobDetail")
    private JobDetail billingNotificationJobDetail;

    @Autowired
    @Qualifier("billingDueDateNotificationJobDetail")
    private JobDetail billingDueDateNotificationJobDetail;

    @Autowired
    @Qualifier("reconFileGeneratorJobDetail")
    private JobDetail reconFileGeneratorJobDetail;

    @Autowired
    @Qualifier("invoiceSendJobDetail")
    private JobDetail invoiceSendJobDetail;

    @PostMapping("/sync/organization")
    public ResponseEntity<String> triggerSyncOrganization() throws SchedulerException, InterruptedException {
        JobDetail jobDetail = organizationDataSyncJobDetail;
        Trigger trigger = JobUtil.createOnDemandTrigger("organizationSyncDataOnDemandJobTrigger");
        String jobUuid = Utility.getUUID();
        jobDetail.getJobDataMap().put("jobkey", jobUuid);
        jobDetail.getJobDataMap().put("joborigin", JOB_ORIGIN_ON_DEMAND);
        JobUtil.startJob(jobDetail, trigger);
        return new ResponseEntity<String>("OK", HttpStatus.OK);
    }

    @PostMapping("/sync/subscription")
    public ResponseEntity<String> triggerSyncSubsription() throws SchedulerException, InterruptedException {
        JobDetail jobDetail = subscriptionDataSyncJobDetail;
        Trigger trigger = JobUtil.createOnDemandTrigger("subscriptionSyncDataOnDemandJobTrigger");
        jobDetail.getJobDataMap().put("joborigin", JOB_ORIGIN_ON_DEMAND);
        JobUtil.startJob(jobDetail, trigger);
        return new ResponseEntity<String>("OK", HttpStatus.OK);
    }

    @PostMapping(path = "/count/usage")
    public ResponseEntity<String> triggerApiUsageCount(@RequestParam MultiValueMap<String, String> paramMap) throws SchedulerException, InterruptedException {
        JobDetail jobDetail = apiUsageJobDetail;
        JobDataMap jdm = jobDetail.getJobDataMap();

        String startDate = paramMap.getFirst("after");
        String endDate = paramMap.getFirst("before");

        if (startDate != null && endDate != null) {
            jdm.put("after", startDate);
            jdm.put("before", endDate);

            Trigger trigger = JobUtil.createOnDemandTrigger("apiUsageDataOnDemandJobTrigger");
            jobDetail.getJobDataMap().put("joborigin", JOB_ORIGIN_ON_DEMAND);
            JobUtil.startJob(jobDetail, trigger);

            return new ResponseEntity<String>("OK", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("Required parameter cannot empty", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/housekeeping/usage")
    public ResponseEntity<String> housekeeping(@RequestParam MultiValueMap<String, String> paramMap) throws SchedulerException, InterruptedException {
        JobDetail jobDetail = housekeepingJobDetail;
        Trigger trigger = JobUtil.createOnDemandTrigger("housekeepingOnDemandJobTrigger");
        jobDetail.getJobDataMap().put("joborigin", JOB_ORIGIN_ON_DEMAND);
        JobUtil.startJob(jobDetail, trigger);
        return new ResponseEntity<String>("OK", HttpStatus.OK);
    }

    @PostMapping(path = "/generate/invoice",
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<String> invoice(
            @RequestParam MultiValueMap<String, String> paramMap) throws SchedulerException, ParseException, InterruptedException, ISOException {
        JobDetail jobDetail = invoiceGeneratorJobDetail;
        JobDataMap jdm = jobDetail.getJobDataMap();
        //String invoiceDate = paramMap.getFirst("date");

        String month = paramMap.getFirst("month");
        String year = paramMap.getFirst("year");

        String invoiceDate = year + "-" + ISOUtil.zeropad(month, 2) + "-" + "01";
        
        

        String invoiceCompany = paramMap.getFirst("orgId");
        if (month != null && year != null) {
            if (invoiceCompany != null) {
                jdm.put("orgId", invoiceCompany);
                System.out.println("Receive request for generate invoice : " + invoiceCompany);
            }
            
            System.out.println("Receive request for generate invoice : " + invoiceDate);
            
            
            jdm.put("date", new SimpleDateFormat("yyyy-MM-dd").parse(invoiceDate));
            Trigger trigger = JobUtil.createOnDemandTrigger("invoiceOnDemandJobTrigger");
            jobDetail.getJobDataMap().put("joborigin", JOB_ORIGIN_ON_DEMAND);
            JobUtil.startJob(jobDetail, trigger);
            return new ResponseEntity<String>("OK", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("Required parameter cannot empty", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/send/invoice",
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<String> invoiceSend(@RequestParam MultiValueMap<String, String> paramMap) throws SchedulerException, InterruptedException {
        JobDetail jobDetail = invoiceSendJobDetail;
        jobDetail.getJobDataMap().put("joborigin", JOB_ORIGIN_ON_DEMAND);
        JobDataMap jdm = jobDetail.getJobDataMap();
        String invoiceCompany = paramMap.getFirst("orgId");
        String year = paramMap.getFirst("year");
        String month = paramMap.getFirst("month");
        if (year != null && month != null) {
            jdm.put("year", Integer.valueOf(year));
            jdm.put("month", Integer.valueOf(month));
            if (invoiceCompany != null) {
                jdm.put("orgId", invoiceCompany);
            }
            Trigger trigger = JobUtil.createOnDemandTrigger("invoiceSendOnDemandJobTrigger");
            jobDetail.getJobDataMap().put("joborigin", JOB_ORIGIN_ON_DEMAND);
            JobUtil.startJob(jobDetail, trigger);
            return new ResponseEntity<String>("OK", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("Required parameter cannot empty", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/subscription/billingschema/notification",
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<String> triggerBillingSchemaSettingNotification() throws SchedulerException, InterruptedException {
        JobDetail jobDetail = billingNotificationJobDetail;
        Trigger trigger = JobUtil.createOnDemandTrigger("billingSchemaNotificationOnDemandJobTrigger");
        jobDetail.getJobDataMap().put("joborigin", JOB_ORIGIN_ON_DEMAND);
        JobUtil.startJob(jobDetail, trigger);
        return new ResponseEntity<String>("OK", HttpStatus.OK);
    }

    @PostMapping(path = "/invoice/duedate/notification",
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<String> invoiceDueDateNotification() throws SchedulerException, InterruptedException {
        JobDetail jobDetail = billingDueDateNotificationJobDetail;
        Trigger trigger = JobUtil.createOnDemandTrigger("billingDueDateNotificationOnDemandJobTrigger");
        jobDetail.getJobDataMap().put("joborigin", JOB_ORIGIN_ON_DEMAND);
        JobUtil.startJob(jobDetail, trigger);
        return new ResponseEntity<String>("OK", HttpStatus.OK);
    }

    @PostMapping(path = "/generate/recon/file",
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public ResponseEntity<String> triggerGenerateReconFile(
            @RequestParam MultiValueMap<String, String> paramMap) throws SchedulerException, ParseException, InterruptedException {
        JobDetail jobDetail = reconFileGeneratorJobDetail;
        String reconDate = paramMap.getFirst("date");
        if (reconDate != null) {
            JobDataMap jdm = jobDetail.getJobDataMap();
            jdm.put("date", LocalDate.parse(reconDate, DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        }
        Trigger trigger = JobUtil.createOnDemandTrigger("reconFileGeneratorOnDemandJobTrigger");
        jobDetail.getJobDataMap().put("joborigin", JOB_ORIGIN_ON_DEMAND);
        JobUtil.startJob(jobDetail, trigger);
        return new ResponseEntity<String>("OK", HttpStatus.OK);
    }

}
