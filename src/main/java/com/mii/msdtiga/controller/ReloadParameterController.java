package com.mii.msdtiga.controller;

import com.mii.msdtiga.service.SystemPropertiesService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author vinch
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/rest/reload")
public class ReloadParameterController {
    
    @Autowired
    private SystemPropertiesService svc;
    
    org.slf4j.Logger log = LoggerFactory.getLogger(ReloadParameterController.class);
    
    @PostMapping("/parameter")
    public ResponseEntity<String> triggerSyncOrganization() {
        int i = svc.reloadParameter();
        log.info("Success reload '" + i + "' system parameters");
        System.out.println("Success reload '" + i + "' system parameters");
        return new ResponseEntity<String>("OK", HttpStatus.OK);
    }
    
}
