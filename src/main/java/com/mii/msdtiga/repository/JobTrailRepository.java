package com.mii.msdtiga.repository;

import com.mii.msdtiga.entity.JobTrail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface JobTrailRepository extends JpaRepository<JobTrail, Long> {

    public List<JobTrail> findBySuccessAndStartDateBetween(boolean jobStatus, LocalDateTime startDate, LocalDateTime endDate);

}
