package com.mii.msdtiga.repository;

import com.mii.msdtiga.entity.SystemProperties;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author vinch
 */
@Repository
public interface SystemPropertiesRepository extends JpaRepository <SystemProperties, Long> {
    
    SystemProperties findByPropertyName(String name);
    
}
