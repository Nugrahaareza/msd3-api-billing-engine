package com.mii.msdtiga.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mii.msdtiga.entity.BillingScheme;

@Repository
public interface BillingSchemeRepository extends JpaRepository<BillingScheme, String> {

}
