package com.mii.msdtiga.repository;

import com.mii.msdtiga.entity.AuthorityPage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorityPageRepository extends JpaRepository<AuthorityPage, Long> {
}
