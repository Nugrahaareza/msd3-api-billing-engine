package com.mii.msdtiga.repository;

import com.mii.msdtiga.entity.Invoice;
import com.mii.msdtiga.entity.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mii.msdtiga.entity.BillingSubscriptionLog;

import java.util.List;

@Repository
public interface BillingSubscriptionLogRepository extends JpaRepository<BillingSubscriptionLog, Long> {

    public List<BillingSubscriptionLog> findByMonthAndYearAndOrganizationAndInvoice(int month,int year,Organization organization, Invoice invoice);

    public void deleteByMonthAndYearAndOrganizationAndInvoice(int month,int year,Organization organization, Invoice invoice);

    public void deleteByMonthAndYearAndInvoice(int month,int year, Invoice invoice);

}
