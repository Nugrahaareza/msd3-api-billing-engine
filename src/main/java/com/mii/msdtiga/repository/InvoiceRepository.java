package com.mii.msdtiga.repository;

import com.mii.msdtiga.entity.Invoice;
import com.mii.msdtiga.entity.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

    public List<Invoice> findByMonthAndYearAndOrganization(int month, int year, Organization organization);

    public Invoice findFirstByMonthAndYearAndOrganizationOrderByRevisionDesc(int month, int year, Organization organization);

    public List<Invoice> findByPaidAndDeleted(boolean paid, boolean deleted);
    
    public List<Invoice> findByYearAndMonthAndDeleted(int year, int month, boolean deleted);
    
    public List<Invoice> findByYearAndMonthAndDeletedAndOrganization(int year, int month, boolean deleted, Organization organization);

}
