package com.mii.msdtiga.repository;

import com.mii.msdtiga.entity.BillingSchemeTemplate;
import com.mii.msdtiga.entity.BillingSchemeTemplateCharge;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author vinch
 */
@Repository
public interface BillingSchemeChargeTemplateRepository extends JpaRepository<BillingSchemeTemplateCharge, Long> {
    
    List<BillingSchemeTemplateCharge> findByBillingSchemeTemplate(BillingSchemeTemplate bs);            
    
}
