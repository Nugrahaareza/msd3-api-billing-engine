package com.mii.msdtiga.repository;

import com.mii.msdtiga.entity.PaymentTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface PaymentTransactionRepository extends JpaRepository<PaymentTransaction, Long> {

    @Query(value = "SELECT * FROM payment_transaction WHERE DATE(system_timestamp) = :timestamp", nativeQuery = true)
    List<PaymentTransaction> findSingleDay(@Param("timestamp") LocalDate date);

}
