package com.mii.msdtiga.repository;

import com.mii.msdtiga.entity.Calls;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface CallsRepository extends JpaRepository<Calls, Long> {

    List<Calls> findByTransactionId(String transactionId);

    @Query(value = "SELECT COUNT(*) FROM calls WHERE DATE(timestamp) = :timestamp AND plan_id = :plan_id AND app_id = :app_id AND dev_org_id = :dev_org_id", nativeQuery = true)
    int countBySubscriptionPeriod(@Param("timestamp") LocalDate date, @Param("plan_id") String planId, @Param("app_id") String appId, @Param("dev_org_id") String devOrgId);

    @Query(value = "SELECT COUNT(*) FROM calls WHERE DATE(timestamp) = :timestamp AND plan_id = :plan_id AND app_id = :app_id AND dev_org_id = :dev_org_id AND status_code IN (:status_codes)", nativeQuery = true)
    int countBySubscriptionPeriodAndStatusCode(@Param("timestamp") LocalDate date, @Param("plan_id") String planId, @Param("app_id") String appId, @Param("dev_org_id") String devOrgId, @Param("status_codes") List<String> statusCodes);
    
    @Query(value = "SELECT COUNT(*) FROM calls WHERE DATE(timestamp) = :timestamp AND plan_id = :plan_id AND app_id = :app_id AND dev_org_id = :dev_org_id AND status_code IN (:status_codes) AND resource_id NOT IN (:resource_id)", nativeQuery = true)
    int countBySubscriptionPeriodAndStatusCodeAndResourceIdNotIn(@Param("timestamp") LocalDate date, @Param("plan_id") String planId, @Param("app_id") String appId, @Param("dev_org_id") String devOrgId, @Param("status_codes") List<String> statusCodes, @Param("resource_id") List<String> resourceId);
    
    @Query(value = "SELECT COUNT(*) FROM calls WHERE DATE(timestamp) = :timestamp AND plan_id = :plan_id AND app_id = :app_id AND dev_org_id = :dev_org_id AND resource_id NOT IN (:resource_id)", nativeQuery = true)
    int countBySubscriptionPeriodAndResourceIdNotIn(@Param("timestamp") LocalDate date, @Param("plan_id") String planId, @Param("app_id") String appId, @Param("dev_org_id") String devOrgId, @Param("resource_id") List<String> resourceId);
    
    @Transactional
    @Modifying
    @Query(value = "DELETE FROM calls WHERE DATE(timestamp) = :timestamp", nativeQuery = true)
    void deleteDataByDate(@Param("timestamp") LocalDate date);
    
    @Transactional
    @Modifying
    @Query(value = "DELETE FROM calls WHERE timestamp BETWEEN :startDate AND :endDate", nativeQuery = true)
    void deleteDataByDate(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

    int countByTimestampBefore(LocalDateTime before);
    
    @Transactional
    @Modifying
    void deleteByTimestampBefore(LocalDateTime before);

}
