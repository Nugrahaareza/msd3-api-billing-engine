package com.mii.msdtiga.repository;

import com.mii.msdtiga.entity.BillingScheme;
import com.mii.msdtiga.entity.Organization;
import com.mii.msdtiga.entity.Subscription;
import com.mii.msdtiga.entity.SubscriptionStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {

    List<Subscription> findByAppIdAndPlanIdAndOrganization(String appId, String planId, Organization organization);

    List<Subscription> findByOrganizationAndSubscriptionStatus(Organization organization, SubscriptionStatus status);

    List<Subscription> findBySubscriptionStatusAndBillingScheme(SubscriptionStatus status, BillingScheme billingSchema);

}
