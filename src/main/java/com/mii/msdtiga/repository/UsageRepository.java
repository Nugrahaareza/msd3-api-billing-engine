package com.mii.msdtiga.repository;

import com.mii.msdtiga.entity.Usage;
import com.mii.msdtiga.entity.UsageId;
import java.time.LocalDate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface UsageRepository extends JpaRepository<Usage, UsageId> {

    @Query(value = "SELECT SUM(usage_count) FROM api_usage WHERE YEAR(period) = :year AND MONTH(period) = :month AND subscription_id = :subscription_id", nativeQuery = true)
    Integer countUsageByMonth(@Param("year") int yearPeriod, @Param("month") int monthPeriod, @Param("subscription_id") long subscriptionId);

    @Transactional
    @Modifying
    void deleteByUsageIdPeriodBefore(LocalDate before);
	
}
