package com.mii.msdtiga.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mii.msdtiga.entity.BillingScheme;
import com.mii.msdtiga.entity.BillingSchemeCharge;

@Repository
public interface BillingSchemeChargeRepository extends JpaRepository<BillingSchemeCharge, Long> {

	@Query(value = "SELECT * FROM billing_scheme_charge WHERE billing_pk = :billing_pk ORDER BY billing_tier_lvl ASC", nativeQuery = true)
	List<BillingSchemeCharge> findChargeByScheme(@Param("billing_pk") String billingSchemeId);
	
	List<BillingSchemeCharge> findByBillingScheme(BillingScheme bs);
}
