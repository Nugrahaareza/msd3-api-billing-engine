package com.mii.msdtiga.job;

import com.mii.msdtiga.service.CallsService;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

/**
 *
 * @author vinch
 */
public class HousekeepingJob extends JobWithTrail {

    @Override
    public void executeJob(JobExecutionContext jobExecutionContext) throws Exception {
        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        CallsService callsService = (CallsService) jobDataMap.get("callsService");
        int countDeletedCalls = callsService.getCallsByDateBefore();
        callsService.deleteCallsByDateBefore();
        callsService.deleteUsageByDateBefore();
        jobDataMap.put("detail", "Total deleted data : " + countDeletedCalls);
    }
    
}
