package com.mii.msdtiga.job;

import com.mii.msdtiga.entity.Organization;
import com.mii.msdtiga.service.InvoiceService;
import com.mii.msdtiga.service.OrganizationService;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

/**
 *
 * @author vinch
 */
public class SendInvoiceJob extends JobWithTrail {

    @Override
    public void executeJob(JobExecutionContext jobExecutionContext) throws Exception {
        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        InvoiceService invoiceService = (InvoiceService) jobDataMap.get("invoiceService");
        OrganizationService organizationService = (OrganizationService) jobDataMap.get("organizationService");
        String organizationId = jobDataMap.getString("orgId");
        int year = jobDataMap.getInt("year");
        int month = jobDataMap.getInt("month");
        int savedData = 0;
        if(organizationId != null) {
            Organization organization = organizationService.findByOrgApimId(organizationId);
            if(organization != null) {
                savedData = invoiceService.sendInvoice(organization, year, month);
            }
        } else {
            savedData = invoiceService.sendInvoice(year, month);
        }
        jobDataMap.put("detail", String.valueOf(savedData));
    }
    
}
