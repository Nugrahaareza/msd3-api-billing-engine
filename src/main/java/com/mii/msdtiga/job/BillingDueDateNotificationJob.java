package com.mii.msdtiga.job;

import com.mii.msdtiga.service.InvoiceService;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

public class BillingDueDateNotificationJob extends JobWithTrail {

    @Override
    public void executeJob(JobExecutionContext jobExecutionContext) throws Exception {
        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        InvoiceService invoiceService = (InvoiceService) jobDataMap.get("invoiceService");
        int savedData = invoiceService.invoiceNotification();
        jobDataMap.put("detail", String.valueOf(savedData));
    }
}
