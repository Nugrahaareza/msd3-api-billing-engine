package com.mii.msdtiga.job;

import com.mii.msdtiga.entity.ApimConnection;
import com.mii.msdtiga.service.ApimConnectionService;
import com.mii.msdtiga.service.SubscriptionService;
import java.util.List;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

public class SubscriptionSyncJob extends JobWithTrail {

    @Override
    public void executeJob(JobExecutionContext jobExecutionContext) throws Exception {
        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        SubscriptionService subscriptionService = (SubscriptionService) jobDataMap.get("subscriptionService");
        subscriptionService.fetchAndStoreMultipleData();
        //jobDataMap.put("detail", String.valueOf(savedData));
        
        List<ApimConnection> list = ApimConnectionService.getAllApimConnections();
        for(ApimConnection apimc : list) {
            
        }
        
        
    }
}
