package com.mii.msdtiga.job;

import com.mii.msdtiga.service.OrganizationService;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

import java.io.IOException;
import java.net.URISyntaxException;

public class OrganizationSyncJob extends JobWithTrail {

    @Override
    public void executeJob(JobExecutionContext jobExecutionContext) throws IOException, URISyntaxException {
        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        OrganizationService organizationService = (OrganizationService) jobDataMap.get("organizationService");
        organizationService.fetchAndStoreMultipleData();
        //jobDataMap.put("detail", String.valueOf(savedData));
    }
}
