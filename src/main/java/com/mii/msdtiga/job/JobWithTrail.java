package com.mii.msdtiga.job;

import com.mii.msdtiga.entity.JobTrail;
import com.mii.msdtiga.service.JobTrailService;
import com.mii.msdtiga.util.JobUtil;
import com.mii.msdtiga.util.Utility;
import org.quartz.*;

import java.time.LocalDateTime;

public abstract class JobWithTrail implements Job {

    public static String JOB_TRIGGER_SOURCE = "QUARTZ_SCHEDULLER";

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        String serviceName = jobDataMap.getString("serviceName");
        JobTrailService svc = (JobTrailService) jobDataMap.get("jobTrailService");
        String jobOrigin = jobDataMap.getString("joborigin");
        JobTrail jb = logStartTask(serviceName, svc, jobOrigin);
        try {
            executeJob(jobExecutionContext);
            jb.setJobDetail(jobDataMap.getString("detail"));
            jb.setSuccess(true);
        } catch(Exception e) {
            jb.setSuccess(false);
            jb.setError(e.getMessage());
            jb.setErrorDetail(Utility.printException(e));
        } finally {
            logEndTask(jb, svc);
        }
    }

    public static JobTrail logStartTask(String serviceName, JobTrailService svc, String origin) {
        JobTrail jobTrail = JobUtil.newJobTrail(JOB_TRIGGER_SOURCE, serviceName);
        jobTrail.setSource(origin);
        svc.save(jobTrail);
        return jobTrail;
    }

    public static void logEndTask(JobTrail jobTrail, JobTrailService svc) {
        jobTrail.setFinishDate(LocalDateTime.now());
        svc.save(jobTrail);
    }

    public abstract void executeJob(JobExecutionContext jobExecutionContext) throws Exception;

}
