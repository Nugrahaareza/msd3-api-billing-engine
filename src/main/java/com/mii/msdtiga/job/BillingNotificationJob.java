package com.mii.msdtiga.job;

import com.mii.msdtiga.service.SubscriptionService;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

public class BillingNotificationJob extends JobWithTrail {

    @Override
    public void executeJob(JobExecutionContext jobExecutionContext) throws Exception {
        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        SubscriptionService subscriptionService = (SubscriptionService) jobDataMap.get("subscriptionService");
        int savedData = subscriptionService.triggerNotification();
        jobDataMap.put("detail", String.valueOf(savedData));
    }
}
