package com.mii.msdtiga.job;

import com.mii.msdtiga.service.PaymentTransactionService;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

import java.time.LocalDate;

public class ReconFileGeneratorJob extends JobWithTrail {

    @Override
    public void executeJob(JobExecutionContext jobExecutionContext) throws Exception {
        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        PaymentTransactionService paymentTransactionService = (PaymentTransactionService) jobDataMap.get("paymentTransactionService");
        LocalDate localDate = (LocalDate) jobDataMap.get("date");
        int totalReconRecord = paymentTransactionService.generateCsv(localDate);
        jobDataMap.put("detail", String.valueOf(totalReconRecord));
    }
}
