package com.mii.msdtiga.job;

import com.mii.msdtiga.entity.ApimConnection;
import com.mii.msdtiga.entity.JobTrail;
import com.mii.msdtiga.service.*;
import com.mii.msdtiga.util.DateUtil;
import com.mii.msdtiga.util.Utility;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.LoggerFactory;
import org.springframework.core.task.TaskExecutor;

public class APIUsageDataSyncJob implements Job {

    static org.slf4j.Logger log = LoggerFactory.getLogger(APIUsageDataSyncJob.class);

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        CallsService callsService = (CallsService) jobDataMap.get("callsService");
        TaskExecutor taskExecutor = (TaskExecutor) jobDataMap.get("threadExecutor");
        String serviceName = jobDataMap.getString("serviceName");
        JobTrailService svc = (JobTrailService) jobDataMap.get("jobTrailService");
        String jobOrigin = jobDataMap.getString("joborigin");

        String after = jobDataMap.getString("after");
        String before = jobDataMap.getString("before");

        List<ApimConnection> list = ApimConnectionService.getAllApimConnections();
        
        for(ApimConnection apimc : list) {
            taskExecutor.execute(() -> {
                JobTrail jb = JobWithTrail.logStartTask(serviceName, svc, jobOrigin);
                int callsCount = 0;
                int usageCount = 0;
                try {
                    callsCount = callsService.fetchAndStoreData(apimc, after, before);
                    Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(after);
                    Date date2 = new SimpleDateFormat("yyyy-MM-dd").parse(before);
                    List<Date> dates = DateUtil.getDatesBetween(date1, date2);
                    for(Date date : dates) {
                        usageCount = usageCount + callsService.countAndStoreUsage(date);
                    }
                } catch (IOException | URISyntaxException | ParseException ex) {
                    log.error(Utility.printException(ex));
                } finally {
                    jb.setJobDetail("Total data for '" + apimc.getOrgName() + "' provider Orgs "
                            + "and Catalog '"+apimc.getCatalogName()+"' "
                            + "are 'Call data saved : " + callsCount + " "
                            + "and usage data saved : " + usageCount + "'");
                    JobWithTrail.logEndTask(jb, svc);
                }
            });
        }
    }
}
