package com.mii.msdtiga.job;

import com.mii.msdtiga.entity.Organization;
import com.mii.msdtiga.service.BillingSubscriptionLogService;
import com.mii.msdtiga.service.InvoiceService;
import com.mii.msdtiga.service.OrganizationService;
import com.mii.msdtiga.util.DateUtil;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InvoiceGeneratorJob extends JobWithTrail {

    static Logger log = LoggerFactory.getLogger(BillingSubscriptionLogService.class);
    
    @Override
    public void executeJob(JobExecutionContext jobExecutionContext) throws Exception {
        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        InvoiceService invoiceService = (InvoiceService) jobDataMap.get("invoiceService");
        OrganizationService organizationService = (OrganizationService) jobDataMap.get("organizationService");
        BillingSubscriptionLogService billingSubscriptionLogService = 
                (BillingSubscriptionLogService) jobDataMap.get("billingSubscriptionLogService");
        Date date = (Date) jobDataMap.get("date");
        String organizationId = jobDataMap.getString("orgId");
        int savedData = 0;
        if(organizationId != null) {
            Organization organization = organizationService.findByOrgApimId(organizationId);
            if(organization != null) {
                System.out.println("Generate Billing for single organization");
                billingSubscriptionLogService.calculateAndStoreBilling(date, organization);
                savedData = invoiceService.generateInvoice(DateUtil.convertDateToLocalDate(date), organization);
            }
        } else {
            System.out.println("Generate Billing for all organization");
            billingSubscriptionLogService.calculateAndStoreBilling(date);
            savedData = invoiceService.generateInvoice(DateUtil.convertDateToLocalDate(date));
        }
        jobDataMap.put("detail", String.valueOf(savedData));
    }
}
