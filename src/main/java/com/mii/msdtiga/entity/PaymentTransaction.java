package com.mii.msdtiga.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class PaymentTransaction {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String paymentBankReferenceNumber;

    @Column
    private String paymentReferenceId;

    @Column(columnDefinition = "DATE")
    private LocalDate paymentDate;

    @Column(columnDefinition = "TIME")
    private LocalTime paymentTime;

    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime systemTimestamp;

    @Column
    private String paymentAuthorizationResponseId;

    @Column
    private BigDecimal paymentAmount;

    @Column
    private String virtualAccountNumber;

    @Column
    private LocalDate invoiceDueDate;

    @Column
    private String organizationName;

    @Column
    private String period;

    @OneToOne
    @JoinColumn
    private Invoice invoice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPaymentBankReferenceNumber() {
        return paymentBankReferenceNumber;
    }

    public void setPaymentBankReferenceNumber(String paymentBankReferenceNumber) {
        this.paymentBankReferenceNumber = paymentBankReferenceNumber;
    }

    public String getPaymentReferenceId() {
        return paymentReferenceId;
    }

    public void setPaymentReferenceId(String paymentReferenceId) {
        this.paymentReferenceId = paymentReferenceId;
    }

    public LocalDate getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDate paymentDate) {
        this.paymentDate = paymentDate;
    }

    public LocalTime getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(LocalTime paymentTime) {
        this.paymentTime = paymentTime;
    }

    public LocalDateTime getSystemTimestamp() {
        return systemTimestamp;
    }

    public void setSystemTimestamp(LocalDateTime systemTimestamp) {
        this.systemTimestamp = systemTimestamp;
    }

    public String getPaymentAuthorizationResponseId() {
        return paymentAuthorizationResponseId;
    }

    public void setPaymentAuthorizationResponseId(String paymentAuthorizationResponseId) {
        this.paymentAuthorizationResponseId = paymentAuthorizationResponseId;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getVirtualAccountNumber() {
        return virtualAccountNumber;
    }

    public void setVirtualAccountNumber(String virtualAccountNumber) {
        this.virtualAccountNumber = virtualAccountNumber;
    }

    public LocalDate getInvoiceDueDate() {
        return invoiceDueDate;
    }

    public void setInvoiceDueDate(LocalDate invoiceDueDate) {
        this.invoiceDueDate = invoiceDueDate;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }
}
