package com.mii.msdtiga.entity;

public interface DetailOrg {

	public String getOrganizationName();
	public String getowner();
	public String getstatus();
	public String getAppName();
	public String getAppStatus();
	public String getProductName();
	public String getProductStatus();
	public String getplanname();
	public String getBillingSchemeId();
	public String getBillingName();
	public String getBillingSchemeType();
	public String getplan_id();
	public int getAppId();
	public int getApprovalStatus();
}
