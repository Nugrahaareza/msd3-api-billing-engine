package com.mii.msdtiga.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class AuthorityPage {

    @Id
    @GeneratedValue
    private long id;

    private long rolePk;

    private long pagesPk;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRolePk() {
        return rolePk;
    }

    public void setRolePk(long rolePk) {
        this.rolePk = rolePk;
    }

    public long getPagesPk() {
        return pagesPk;
    }

    public void setPagesPk(long pagesPk) {
        this.pagesPk = pagesPk;
    }





}
