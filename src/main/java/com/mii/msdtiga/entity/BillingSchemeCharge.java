package com.mii.msdtiga.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "billing_scheme_charge")
public class BillingSchemeCharge implements Serializable{
	
	@Id
	@Column(name = "id")
	@GeneratedValue
        private long id;
	
	@Column(name = "billing_charge")
	private long billingCharge;
	
	@Column(name = "billing_tier_lvl")
	private int billingTierLvl;
	
	
	@Column(name = "min_call",nullable = true)
	private int minCall ;
	
	@Column(name = "max_call", nullable = true)
	private int maxCall ;
	
	@ManyToOne(targetEntity = BillingScheme.class, fetch = FetchType.EAGER, cascade = { CascadeType.ALL,
			CascadeType.MERGE, CascadeType.REFRESH })
        @JoinColumn(name = "billing_pk")
	private BillingScheme billingScheme;
	
	public long getBillingChargeId() {
		return id;
	}
	
	public void setBillingChargeId(long id) {
		this.id = id;
	}

	public long getBillingCharge() {
		return billingCharge;
	}

	public void setBillingcharge(long billingCharge) {
		this.billingCharge = billingCharge;
	}

	public int getBillinglvl() {
		return billingTierLvl;
	}

	public void setBillinglvl(int billingTierLvl) {
		this.billingTierLvl = billingTierLvl;
	}

	public int getMinCall() {
		return minCall;
	}

	public void setMinCall(int minCall) {
		this.minCall = minCall;
	}

	public int getMaxCall() {
		return maxCall;
	}

	public void setMaxCall(int maxCall) {
		this.maxCall = maxCall;
	}

	public BillingScheme getBillingPk() {
		return billingScheme;
	}

	public void setBillingpk(BillingScheme billingPk) {
		this.billingScheme = billingPk;
	}


}
