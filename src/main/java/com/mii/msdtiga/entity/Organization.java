package com.mii.msdtiga.entity;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class Organization {

    @Id
    @GeneratedValue
    private int organizationId;

    @Column(unique = true)
    private String orgApimId;

    @Column
    private String organizationName;

    @Column
    private String organizationStatus;

    @Column
    private String organizationUrl;

    @Column
    private String ownerUsername;

    @Column
    private String ownerEmail;

    @Column
    private String notificationEmail;

    @Column
    private String notificationName;

    @Column
    private String providerOrgName;

    @Column
    private String catalogName;
    
    @Column(columnDefinition = "integer default 1")
    private Integer approvalStatus;

    public int getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrgApimId() {
        return orgApimId;
    }

    public void setOrgApimId(String orgApimId) {
        this.orgApimId = orgApimId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationStatus() {
        return organizationStatus;
    }

    public void setOrganizationStatus(String organizationStatus) {
        this.organizationStatus = organizationStatus;
    }

    public String getOrganizationUrl() {
        return organizationUrl;
    }

    public void setOrganizationUrl(String organizationUrl) {
        this.organizationUrl = organizationUrl;
    }

    public String getOwnerUsername() {
        return ownerUsername;
    }

    public void setOwnerUsername(String ownerUsername) {
        this.ownerUsername = ownerUsername;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public String getNotificationEmail() {
        return notificationEmail;
    }

    public void setNotificationEmail(String notificationEmail) {
        this.notificationEmail = notificationEmail;
    }

    public String getNotificationName() {
        return notificationName;
    }

    public void setNotificationName(String notificationName) {
        this.notificationName = notificationName;
    }

    /**
     * @return the providerOrgName
     */
    public String getProviderOrgName() {
        return providerOrgName;
    }

    /**
     * @param providerOrgName the providerOrgName to set
     */
    public void setProviderOrgName(String providerOrgName) {
        this.providerOrgName = providerOrgName;
    }

    /**
     * @return the catalogName
     */
    public String getCatalogName() {
        return catalogName;
    }

    /**
     * @param catalogName the catalogName to set
     */
    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }

    /**
     * @return the approvalStatus
     */
    public Integer getApprovalStatus() {
        return approvalStatus;
    }

    /**
     * @param approvalStatus the approvalStatus to set
     */
    public void setApprovalStatus(Integer approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

}
