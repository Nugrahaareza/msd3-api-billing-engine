package com.mii.msdtiga.entity;

public enum SubscriptionStatus {

    NOT_SET,
    SET,
    SET_NOT_REQUIRED;

}
