package com.mii.msdtiga.entity;

import java.io.Serializable;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "billing_scheme")
public class BillingScheme implements Serializable {

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "scheme_name")
    private String schemeName;

    @Column(name = "scheme_type")
    private String schemeType;

    @Column(name = "approval_status")
    private int schemeApprovalStatus;

    @Column
    private String comment;

    @ManyToOne(targetEntity = BillingSchemeTemplate.class, fetch = FetchType.EAGER, cascade = {CascadeType.ALL, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "billing_template_pk", referencedColumnName = "billingTemplateId")
    private BillingSchemeTemplate billingSchemeTemplate;

    public long getBillingSchemeId() {
        return id;
    }

    public void setBillingSchemeId(long id) {
        this.id = id;
    }

    public String getBillingSchemeName() {
        return schemeName;
    }

    public void setBillingSchemeName(String schemeName) {
        this.schemeName = schemeName;
    }

    public String getBillingSchemeType() {
        return schemeType;
    }

    public void setBillingSchemeType(String schemeType) {
        this.schemeType = schemeType;
    }

    public int getBillingSchemeApprovalStatus() {
        return schemeApprovalStatus;
    }

    public void setBillingSchemeApprovalStatus(int schemeApprovalStatus) {
        this.schemeApprovalStatus = schemeApprovalStatus;
    }

    /**
     * @return the billingSchemeTemplate
     */
    public BillingSchemeTemplate getBillingSchemeTemplate() {
        return billingSchemeTemplate;
    }

    /**
     * @param billingSchemeTemplate the billingSchemeTemplate to set
     */
    public void setBillingSchemeTemplate(BillingSchemeTemplate billingSchemeTemplate) {
        this.billingSchemeTemplate = billingSchemeTemplate;
    }

}
