package com.mii.msdtiga.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class Calls {

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    private String transactionId;

    @Column
    @DateTimeFormat(pattern="dd/MM/yyyy hh:mm:ss")
    private LocalDateTime timestamp;

    @Column
    private String appName;

    @Column
    private String appId;

    @Column
    private String catalogName;

    @Column
    private String planName;

    @Column
    private String planId;

    @Column
    private String productName;

    @Column
    private String devOrgName;

    @Column
    private String devOrgId;

    @Column
    private String uriPath;

    @Column
    private String timeToServe;

    @Column
    @Lob
    private String queryString;

    @Column
    private String statusCode;

    @Column
    private String userAgent;

    @Column
    @Lob
    private String requestBody;

    @Column
    @Lob
    private String responseBody;

    @Column
    private String host;

    @Column
    private String clientIp;

    @Column
    private String gatewayIp;
    
    @Column
    private String resourceId;

    @Column
    private String logPolicy;
    
    @Column
    private String orgName;

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getCatalogName() {
        return catalogName;
    }

    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDevOrgName() {
        return devOrgName;
    }

    public void setDevOrgName(String devOrgName) {
        this.devOrgName = devOrgName;
    }

    public String getUriPath() {
        return uriPath;
    }

    public void setUriPath(String uriPath) {
        this.uriPath = uriPath;
    }

    public String getTimeToServe() {
        return timeToServe;
    }

    public void setTimeToServe(String timeToServe) {
        this.timeToServe = timeToServe;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getGatewayIp() {
        return gatewayIp;
    }

    public void setGatewayIp(String gatewayIp) {
        this.gatewayIp = gatewayIp;
    }

    public String getLogPolicy() {
        return logPolicy;
    }

    public void setLogPolicy(String logPolicy) {
        this.logPolicy = logPolicy;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDevOrgId() {
        return devOrgId;
    }

    public void setDevOrgId(String devOrgId) {
        this.devOrgId = devOrgId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    /**
     * @return the resourceId
     */
    public String getResourceId() {
        return resourceId;
    }

    /**
     * @param resourceId the resourceId to set
     */
    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    /**
     * @return the orgName
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * @param orgName the orgName to set
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }
}
