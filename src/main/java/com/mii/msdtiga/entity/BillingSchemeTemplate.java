package com.mii.msdtiga.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author vinch
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class BillingSchemeTemplate {

    @Id
    @GeneratedValue
    private long billingTemplateId;

    @Column
    private String billingSchemeTemplateName;

    @Column
    private String billingSchemeTemplateType;

    @Column
    private int billingTemplateApprovalStatus;

    @Column
    private boolean isDeleted;

    /**
     * @return the billingTemplateId
     */
    public long getBillingTemplateId() {
        return billingTemplateId;
    }

    /**
     * @param billingTemplateId the billingTemplateId to set
     */
    public void setBillingTemplateId(long billingTemplateId) {
        this.billingTemplateId = billingTemplateId;
    }

    /**
     * @return the billingSchemeTemplateName
     */
    public String getBillingSchemeTemplateName() {
        return billingSchemeTemplateName;
    }

    /**
     * @param billingSchemeTemplateName the billingSchemeTemplateName to set
     */
    public void setBillingSchemeTemplateName(String billingSchemeTemplateName) {
        this.billingSchemeTemplateName = billingSchemeTemplateName;
    }

    /**
     * @return the billingSchemeTemplateType
     */
    public String getBillingSchemeTemplateType() {
        return billingSchemeTemplateType;
    }

    /**
     * @param billingSchemeTemplateType the billingSchemeTemplateType to set
     */
    public void setBillingSchemeTemplateType(String billingSchemeTemplateType) {
        this.billingSchemeTemplateType = billingSchemeTemplateType;
    }

    /**
     * @return the billingTemplateApprovalStatus
     */
    public int getBillingTemplateApprovalStatus() {
        return billingTemplateApprovalStatus;
    }

    /**
     * @param billingTemplateApprovalStatus the billingTemplateApprovalStatus to set
     */
    public void setBillingTemplateApprovalStatus(int billingTemplateApprovalStatus) {
        this.billingTemplateApprovalStatus = billingTemplateApprovalStatus;
    }

    /**
     * @return the isDeleted
     */
    public boolean isIsDeleted() {
        return isDeleted;
    }

    /**
     * @param isDeleted the isDeleted to set
     */
    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

}
