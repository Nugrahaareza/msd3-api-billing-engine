package com.mii.msdtiga.entity;

import java.io.Serializable;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class UserEntity implements Serializable {

	@Id
	@GeneratedValue
	private long id;


	@Column(nullable = false, unique = true)
	private String username;

	@Column
	private String password;

	@Column
	private String email;


	@ManyToOne(targetEntity = Role.class)
	@JoinColumn(name="rolePk", referencedColumnName = "id" )
	private Role roleEnt;


	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public Role getRoleEnt() {
		return roleEnt;
	}
	public void setRoleEnt(Role roleEnt) {
		this.roleEnt = roleEnt;
	}
}
