package com.mii.msdtiga.entity;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "subscription")
public class Subscription {

    public SubscriptionStatus getBillingStatus() {
        return subscriptionStatus;
    }

    public void setBillingStatus(SubscriptionStatus subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    private String subscriptionId;

    @ManyToOne(targetEntity = Organization.class)
    @JoinColumn(name = "org_pk", referencedColumnName = "organizationId")
    private Organization organization;

    @ManyToOne(targetEntity = BillingScheme.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "billing_pk", referencedColumnName = "id")
    private BillingScheme billingScheme;

    private String appId;

    private String appName;

    private String productId;

    private String productName;

    private String planId;

    private String planName;

    private SubscriptionStatus subscriptionStatus;

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public BillingScheme getBilllingScheme() {
        return billingScheme;
    }

    public void setBillingScheme(BillingScheme billingScheme) {
        this.billingScheme = billingScheme;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
