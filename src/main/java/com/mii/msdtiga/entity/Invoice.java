package com.mii.msdtiga.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class Invoice {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private int month;

    @Column
    private int year;

    @ManyToOne(targetEntity = Organization.class)
    @JoinColumn(name="orgPk", referencedColumnName = "organizationId")
    private Organization organization;

    @Column
    private int revision;

    @Column
    private LocalDateTime generateTimestamp;

    @Column
    private LocalDate invoiceDate;

    @Column
    private boolean paid;

    @Column
    private String filePath;

    @Column
    private String virtualAccountNumber;

    @Column
    private String authorizationId;

    @Column
    private BigDecimal totalAmount;
    
    @Column
    private BigDecimal ppnAmount;
    
    @Column
    private BigDecimal pphAmount;
    
    @Column
    private BigDecimal grandTotalAmount;
    

    @OneToOne(mappedBy = "invoice", cascade = CascadeType.ALL)
    private PaymentTransaction paymentTransaction;

    @Column(columnDefinition="tinyint(1) default 0")
    private boolean deleted;

    @Column
    private LocalDate dueDate;

    @Column
    private LocalDateTime paymentDateTime;
    
    @Column(columnDefinition="int(11) default 0")
    private int notificationCounter;
    
    @Column(columnDefinition="int(11) default 0")
    private int sentCounter;
    
    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getVirtualAccountNumber() {
        return virtualAccountNumber;
    }

    public void setVirtualAccountNumber(String virtualAccountNumber) {
        this.virtualAccountNumber = virtualAccountNumber;
    }

    public PaymentTransaction getPaymentTransaction() {
        return paymentTransaction;
    }

    public void setPaymentTransaction(PaymentTransaction paymentTransaction) {
        this.paymentTransaction = paymentTransaction;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getAuthorizationId() {
        return authorizationId;
    }

    public void setAuthorizationId(String authorizationId) {
        this.authorizationId = authorizationId;
    }

    public LocalDateTime getGenerateTimestamp() {
        return generateTimestamp;
    }

    public void setGenerateTimestamp(LocalDateTime generateTimestamp) {
        this.generateTimestamp = generateTimestamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public LocalDateTime getPaymentDateTime() {
        return paymentDateTime;
    }

    public void setPaymentDateTime(LocalDateTime paymentDateTime) {
        this.paymentDateTime = paymentDateTime;
    }

    public LocalDate getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(LocalDate invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    /**
     * @return the remindingCounter
     */
    public int getRemindingCounter() {
        return notificationCounter;
    }

    /**
     * @param remindingCounter the remindingCounter to set
     */
    public void setRemindingCounter(int remindingCounter) {
        this.notificationCounter = remindingCounter;
    }

    /**
     * @return the sentCounter
     */
    public int getSentCounter() {
        return sentCounter;
    }

    /**
     * @param sentCounter the sentCounter to set
     */
    public void setSentCounter(int sentCounter) {
        this.sentCounter = sentCounter;
    }
    
    public BigDecimal getPpnAmount() {
        return ppnAmount;
    }

    public void setPpnAmount(BigDecimal ppnAmount) {
        this.ppnAmount = ppnAmount;
    }
    
    public BigDecimal getPphAmount() {
        return pphAmount;
    }

    public void setPphAmount(BigDecimal pphAmount) {
        this.pphAmount = pphAmount;
    }
    
    public BigDecimal getGrandTotalAmount() {
        return grandTotalAmount;
    }

    public void setGrandTotalAmount(BigDecimal grandTotalAmount) {
        this.grandTotalAmount = grandTotalAmount;
    }
}
