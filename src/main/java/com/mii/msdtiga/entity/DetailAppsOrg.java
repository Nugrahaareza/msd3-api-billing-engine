package com.mii.msdtiga.entity;

public interface DetailAppsOrg {

    public String getAppsName();
    public String getOrganizationName();
    public String getProductTitle();
    public String getPlantitle();
    public String getSubscriptionId();
    public String getBillingName();
    public String getBillingSchemeType();
    public String getBillingSchemeId();

}
