package com.mii.msdtiga.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author vinch
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class BillingSchemeTemplateCharge {

    @Id
    @GeneratedValue
    private long billingTemplateChargeId;

    @Column
    private int billingTemplateCharge;

    @Column
    private String billingTemplateLvl;

    @Column(nullable = true)
    private int minCall;

    @Column
    private int maxCall;

    @ManyToOne(targetEntity = BillingSchemeTemplate.class)
    @JoinColumn(name = "billingTemplatePk", referencedColumnName = "billingTemplateId")
    private BillingSchemeTemplate billingSchemeTemplate;

    /**
     * @return the billingTemplateChargeId
     */
    public long getBillingTemplateChargeId() {
        return billingTemplateChargeId;
    }

    /**
     * @param billingTemplateChargeId the billingTemplateChargeId to set
     */
    public void setBillingTemplateChargeId(long billingTemplateChargeId) {
        this.billingTemplateChargeId = billingTemplateChargeId;
    }

    /**
     * @return the billingTemplateCharge
     */
    public int getBillingTemplateCharge() {
        return billingTemplateCharge;
    }

    /**
     * @param billingTemplateCharge the billingTemplateCharge to set
     */
    public void setBillingTemplateCharge(int billingTemplateCharge) {
        this.billingTemplateCharge = billingTemplateCharge;
    }

    /**
     * @return the billingTemplateLvl
     */
    public String getBillingTemplateLvl() {
        return billingTemplateLvl;
    }

    /**
     * @param billingTemplateLvl the billingTemplateLvl to set
     */
    public void setBillingTemplateLvl(String billingTemplateLvl) {
        this.billingTemplateLvl = billingTemplateLvl;
    }

    /**
     * @return the minCall
     */
    public int getMinCall() {
        return minCall;
    }

    /**
     * @param minCall the minCall to set
     */
    public void setMinCall(int minCall) {
        this.minCall = minCall;
    }

    /**
     * @return the maxCall
     */
    public int getMaxCall() {
        return maxCall;
    }

    /**
     * @param maxCall the maxCall to set
     */
    public void setMaxCall(int maxCall) {
        this.maxCall = maxCall;
    }

    /**
     * @return the billingSchemeTemplate
     */
    public BillingSchemeTemplate getBillingSchemeTemplate() {
        return billingSchemeTemplate;
    }

    /**
     * @param billingSchemeTemplate the billingSchemeTemplate to set
     */
    public void setBillingSchemeTemplate(BillingSchemeTemplate billingSchemeTemplate) {
        this.billingSchemeTemplate = billingSchemeTemplate;
    }

}
