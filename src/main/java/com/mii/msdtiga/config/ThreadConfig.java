
package com.mii.msdtiga.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;


@Configuration
public class ThreadConfig {
    
    @Value("${thread.corepool.size}")
    int corePoolSize;
    
    @Value("${thread.maxpool.size}")
    int maxPoolSize;
    
    @Bean
    public TaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setThreadNamePrefix("apim_task_executor_thread");
        executor.initialize();
        return executor;
    }

    
}
