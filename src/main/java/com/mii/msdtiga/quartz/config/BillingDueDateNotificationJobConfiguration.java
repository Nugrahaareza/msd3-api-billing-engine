package com.mii.msdtiga.quartz.config;

import com.mii.msdtiga.job.BillingDueDateNotificationJob;
import com.mii.msdtiga.service.InvoiceService;
import com.mii.msdtiga.service.JobTrailService;
import com.mii.msdtiga.service.SystemPropertiesService;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BillingDueDateNotificationJobConfiguration {

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private JobTrailService jobTrailService;
    
    //@Autowired
    //private SystemPropertiesService systemPropertiesService;

    @Bean
    public JobDetail billingDueDateNotificationJobDetail() {
        JobDetail jobDetail = JobBuilder.newJob(BillingDueDateNotificationJob.class).withIdentity("BillingDueDateNotificationJob")
                .storeDurably().build();
        JobDataMap jdm = jobDetail.getJobDataMap();
        jdm.put("jobTrailService", jobTrailService);
        jdm.put("invoiceService", invoiceService);
        jdm.put("serviceName", "Due Date Notification SVC");
        jdm.put("joborigin", "SCHEDULLER");
        return jobDetail;
    }

    @Bean
    public Trigger billingDueDateNotificationTrigger(@Qualifier("billingDueDateNotificationJobDetail") JobDetail billingDueDateNotificationJobDetail) {

        return TriggerBuilder.newTrigger().forJob(billingDueDateNotificationJobDetail())

                .withIdentity("billingDueDateNotificationTrigger")
                .withSchedule(CronScheduleBuilder.cronSchedule(SystemPropertiesService.getPropertiesString("job.cron.duedate.notif")))
                .build();
    }

}
