package com.mii.msdtiga.quartz.config;

import com.mii.msdtiga.job.InvoiceGeneratorJob;
import com.mii.msdtiga.service.BillingSubscriptionLogService;
import com.mii.msdtiga.service.InvoiceService;
import com.mii.msdtiga.service.JobTrailService;
import com.mii.msdtiga.service.OrganizationService;
import com.mii.msdtiga.service.SystemPropertiesService;
import com.mii.msdtiga.util.DateUtil;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InvoiceGeneratorJobConfiguration {

    @Autowired
    private JobTrailService jobTrailService;

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private OrganizationService organizationService;
    
    @Autowired
    private BillingSubscriptionLogService billingService;

    @Bean
    public JobDetail invoiceGeneratorJobDetail() {
        JobDetail jobDetail = JobBuilder.newJob(InvoiceGeneratorJob.class).withIdentity("InvoiceGeneratorJob")
                .storeDurably().build();
        JobDataMap jdm = jobDetail.getJobDataMap();
        jdm.put("jobTrailService", jobTrailService);
        jdm.put("invoiceService", invoiceService);
        jdm.put("organizationService", organizationService);
        jdm.put("billingSubscriptionLogService", billingService);
        jdm.put("date", DateUtil.getLastMonth());
        jdm.put("serviceName", "Invoice Generator SVC");
        jdm.put("joborigin", "SCHEDULLER");
        return jobDetail;
    }

    @Bean
    public Trigger invoiceGeneratorTrigger(@Qualifier("invoiceGeneratorJobDetail") JobDetail invoiceGeneratorJobDetail) {

        return TriggerBuilder.newTrigger().forJob(invoiceGeneratorJobDetail())

                .withIdentity("invoiceGeneratorTrigger")
                .withSchedule(CronScheduleBuilder.cronSchedule(SystemPropertiesService.getPropertiesString("job.cron.invoice")))
                .build();
    }

}
