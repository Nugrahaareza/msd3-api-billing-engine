package com.mii.msdtiga.quartz.config;

import com.mii.msdtiga.job.HousekeepingJob;
import com.mii.msdtiga.service.CallsService;
import com.mii.msdtiga.service.JobTrailService;
import com.mii.msdtiga.service.SystemPropertiesService;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author vinch
 */
@Configuration
public class HousekeepingJobConfiguration {
    
    @Autowired
    private JobTrailService jobTrailService;
    
    @Autowired
    private CallsService callService;
    
    @Bean
    public JobDetail housekeepingJobDetail() {
        JobDetail jobDetail = JobBuilder.newJob(HousekeepingJob.class).withIdentity("HousekeepingJob")
                .storeDurably().build();
        JobDataMap jdm = jobDetail.getJobDataMap();
        jdm.put("jobTrailService", jobTrailService);
        jdm.put("callsService", callService);
        jdm.put("serviceName", "Housekeeping SVC");
        jdm.put("joborigin", "SCHEDULLER");
        return jobDetail;
    }

//    @Bean
//    public Trigger housekeepingTrigger(@Qualifier("housekeepingJobDetail") JobDetail housekeepingJobDetail) {
//
//        return TriggerBuilder.newTrigger().forJob(housekeepingJobDetail())
//
//                .withIdentity("housekeepingTrigger")
//                .withSchedule(CronScheduleBuilder.cronSchedule(SystemPropertiesService.getPropertiesString("job.cron.housekeeping")))
//                .build();
//    }
    
}
