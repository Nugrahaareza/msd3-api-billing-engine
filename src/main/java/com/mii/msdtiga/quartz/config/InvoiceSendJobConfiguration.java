package com.mii.msdtiga.quartz.config;

import com.mii.msdtiga.job.SendInvoiceJob;
import com.mii.msdtiga.service.InvoiceService;
import com.mii.msdtiga.service.JobTrailService;
import com.mii.msdtiga.service.OrganizationService;
import com.mii.msdtiga.service.SystemPropertiesService;
import com.mii.msdtiga.util.DateUtil;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author vinch
 */
@Configuration
public class InvoiceSendJobConfiguration {

    @Autowired
    private JobTrailService jobTrailService;

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private OrganizationService organizationService;

    @Bean
    public JobDetail invoiceSendJobDetail() {
        JobDetail jobDetail = JobBuilder.newJob(SendInvoiceJob.class).withIdentity("InvoiceSendJob")
                .storeDurably().build();
        JobDataMap jdm = jobDetail.getJobDataMap();
        jdm.put("jobTrailService", jobTrailService);
        jdm.put("invoiceService", invoiceService);
        jdm.put("organizationService", organizationService);
        jdm.put("year", DateUtil.getYear(DateUtil.getLastMonth()));
        jdm.put("month", DateUtil.getMonth(DateUtil.getLastMonth()));
        jdm.put("serviceName", "Invoice Send SVC");
        jdm.put("joborigin", "SCHEDULLER");
        return jobDetail;
    }

//    @Bean
//    public Trigger invoiceSendTrigger(@Qualifier("invoiceSendJobDetail") JobDetail invoiceSendOnDemandJobDetail) {
//        return TriggerBuilder.newTrigger().forJob(invoiceSendJobDetail())
//                .withIdentity("invoiceSendJobTrigger")
//                .withSchedule(CronScheduleBuilder.cronSchedule(SystemPropertiesService.getPropertiesString("job.cron.invoice.send")))
//                .build();
//    }

}
