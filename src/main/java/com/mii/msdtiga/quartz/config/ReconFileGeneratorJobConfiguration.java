package com.mii.msdtiga.quartz.config;

import com.mii.msdtiga.job.*;
import com.mii.msdtiga.service.JobTrailService;
import com.mii.msdtiga.service.PaymentTransactionService;
import com.mii.msdtiga.service.SystemPropertiesService;
import com.mii.msdtiga.util.DateUtil;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ReconFileGeneratorJobConfiguration {

    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @Autowired
    private JobTrailService jobTrailService;
    
    //@Autowired
    //private SystemPropertiesService systemPropertiesService;

    @Bean
    public JobDetail reconFileGeneratorJobDetail() {
        JobDetail jobDetail = JobBuilder.newJob(ReconFileGeneratorJob.class).withIdentity("ReconFileGeneratorJob")
                .storeDurably().build();
        JobDataMap jdm = jobDetail.getJobDataMap();
        jdm.put("jobTrailService", jobTrailService);
        jdm.put("paymentTransactionService", paymentTransactionService);
        jdm.put("date", DateUtil.getYesterdayLocalDate());
        jdm.put("serviceName", "Recon File SVC");
        jdm.put("joborigin", "SCHEDULLER");
        return jobDetail;
    }

    @Bean
    public Trigger reconFileGeneratorTrigger(@Qualifier("reconFileGeneratorJobDetail") JobDetail reconFileGeneratorJobDetail) {

        return TriggerBuilder.newTrigger().forJob(reconFileGeneratorJobDetail())

                .withIdentity("reconFileGeneratorTrigger")
                .withSchedule(CronScheduleBuilder.cronSchedule(SystemPropertiesService.getPropertiesString("job.cron.recon.file")))
                .build();
    }

}
