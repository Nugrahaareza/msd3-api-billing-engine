package com.mii.msdtiga.quartz.config;

import com.mii.msdtiga.job.BillingNotificationJob;
import com.mii.msdtiga.service.JobTrailService;
import com.mii.msdtiga.service.SubscriptionService;
import com.mii.msdtiga.service.SystemPropertiesService;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BillingNotificationJobConfiguration {

    @Autowired
    private JobTrailService jobTrailService;

    @Autowired
    private SubscriptionService subscriptionService;
    
    //@Autowired
    //private SystemPropertiesService systemPropertiesService;

    @Bean
    public JobDetail billingNotificationJobDetail() {
        JobDetail jobDetail = JobBuilder.newJob(BillingNotificationJob.class).withIdentity("BillingNotificationJob")
                .storeDurably().build();
        JobDataMap jdm = jobDetail.getJobDataMap();
        jdm.put("jobTrailService", jobTrailService);
        jdm.put("subscriptionService", subscriptionService);
        jdm.put("serviceName", "Subscription Billing Schema Setup Notification SVC");
        jdm.put("joborigin", "SCHEDULLER");
        return jobDetail;
    }

    @Bean
    public Trigger billingNotificationTrigger(@Qualifier("billingNotificationJobDetail") JobDetail billingNotificationJobDetail) {

        return TriggerBuilder.newTrigger().forJob(billingNotificationJobDetail())

                .withIdentity("billingNotificationTrigger")
                .withSchedule(CronScheduleBuilder.cronSchedule(SystemPropertiesService.getPropertiesString("job.cron.billing.notif")))
                .build();
    }

}
