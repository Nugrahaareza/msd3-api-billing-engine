package com.mii.msdtiga.quartz.config;

import com.mii.msdtiga.job.SubscriptionSyncJob;
import com.mii.msdtiga.service.JobTrailService;
import com.mii.msdtiga.service.SubscriptionService;
import com.mii.msdtiga.service.SystemPropertiesService;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SubscriptionJobConfiguration {

    @Autowired
    private JobTrailService jobTrailService;

    @Autowired
    private SubscriptionService subscriptionService;
    
    //@Autowired
    //private SystemPropertiesService systemPropertiesService;

    @Bean
    public JobDetail subscriptionDataSyncJobDetail() {
        JobDetail jobDetail = JobBuilder.newJob(SubscriptionSyncJob.class).withIdentity("SubscriptionDataSyncJobDetail")
                .storeDurably().build();
        JobDataMap jdm = jobDetail.getJobDataMap();
        jdm.put("jobTrailService", jobTrailService);
        jdm.put("subscriptionService", subscriptionService);
        jdm.put("serviceName", "Subscription SVC");
        jdm.put("joborigin", "SCHEDULLER");
        return jobDetail;
    }

    @Bean
    public Trigger subscriptionDataSyncTrigger(@Qualifier("subscriptionDataSyncJobDetail") JobDetail subscriptionDataSyncJobDetail) {

        return TriggerBuilder.newTrigger().forJob(subscriptionDataSyncJobDetail)

                .withIdentity("subscriptionDataSyncTrigger")
                .withSchedule(CronScheduleBuilder.cronSchedule(SystemPropertiesService.getPropertiesString("job.cron.subscriptions")))
                .build();
    }

}
