package com.mii.msdtiga.quartz.config;

import com.mii.msdtiga.job.OrganizationSyncJob;
import com.mii.msdtiga.service.JobTrailService;
import com.mii.msdtiga.service.OrganizationService;
import com.mii.msdtiga.service.SystemPropertiesService;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OrganizationJobConfiguration {

    @Autowired
    private JobTrailService jobTrailService;

    @Autowired
    private OrganizationService organizationService;
    
    //@Autowired
    //private SystemPropertiesService systemPropertiesService;

    @Bean
    public JobDetail organizationDataSyncJobDetail() {
        JobDetail jobDetail = JobBuilder.newJob(OrganizationSyncJob.class).withIdentity("OrganizationDataSyncJobDetail")
                .storeDurably().build();
        JobDataMap jdm = jobDetail.getJobDataMap();
        jdm.put("jobTrailService", jobTrailService);
        jdm.put("organizationService", organizationService);
        jdm.put("serviceName", "Organization SVC");
        jdm.put("joborigin", "SCHEDULLER");
        return jobDetail;
    }

    @Bean
    public Trigger organizationDataSyncCronTrigger(@Qualifier("organizationDataSyncJobDetail") JobDetail organizationDataSyncJobDetail) {

        return TriggerBuilder.newTrigger().forJob(organizationDataSyncJobDetail)

                .withIdentity("organizationDataSyncCronTrigger")
                .withSchedule(CronScheduleBuilder.cronSchedule(SystemPropertiesService.getPropertiesString("job.cron.organizations")))
                .build();
    }

}
