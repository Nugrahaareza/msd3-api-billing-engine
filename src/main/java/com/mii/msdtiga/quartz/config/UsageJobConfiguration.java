package com.mii.msdtiga.quartz.config;

import com.mii.msdtiga.job.APIUsageDataSyncJob;
import com.mii.msdtiga.service.CallsService;
import com.mii.msdtiga.service.JobTrailService;
import com.mii.msdtiga.service.SystemPropertiesService;
import com.mii.msdtiga.service.UsageService;
import com.mii.msdtiga.util.DateUtil;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;

@Configuration
public class UsageJobConfiguration {

    @Autowired
    private JobTrailService jobTrailService;

    @Autowired
    private UsageService usageService;
    
    @Autowired
    private CallsService callService;
    
    @Autowired
    private TaskExecutor taskExecutor;
    
    //@Autowired
    //private SystemPropertiesService systemPropertiesService;

    @Bean
    public JobDetail apiUsageJobDetail() {
        JobDetail jobDetail = JobBuilder.newJob(APIUsageDataSyncJob.class).withIdentity("APIUsageJobDetail")
                .storeDurably().build();
        JobDataMap jdm = jobDetail.getJobDataMap();
        jdm.put("jobTrailService", jobTrailService);
        jdm.put("usageService", usageService);
        jdm.put("callsService", callService);
        jdm.put("threadExecutor", taskExecutor);
        //jdm.put("date", DateUtil.getYesterdayDate());
        jdm.put("startDate", DateUtil.getYesterdayDate());
        jdm.put("endDate", DateUtil.getTodayDate());
        jdm.put("serviceName", "API Usage SVC");
        jdm.put("joborigin", "SCHEDULLER");
        return jobDetail;
    }

    @Bean
    public Trigger apiUsageDataSyncTrigger(@Qualifier("apiUsageJobDetail") JobDetail apiUsageJobDetail) {

        return TriggerBuilder.newTrigger().forJob(apiUsageJobDetail)

                .withIdentity("apiUsageDataSyncTrigger")
                .withSchedule(CronScheduleBuilder.cronSchedule(SystemPropertiesService.getPropertiesString("job.cron.usage")))
                .build();
    }

}
