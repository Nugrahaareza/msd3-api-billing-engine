package com.mii.msdtiga;

import com.mii.msdtiga.util.HttpMessage;
import com.mii.msdtiga.util.HttpUtil;
import com.mii.msdtiga.util.SecurityUtil;
import com.mii.msdtiga.util.Utility;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class APIRequestFactory {

    static final org.slf4j.Logger log = LoggerFactory.getLogger(APIRequestFactory.class);

    public static void main(String[] args) {
        /*URIBuilder uri = new URIBuilder();
        uri.setScheme("https");
        uri.setHost("apimgmtdev.banksinarmas.com");
        uri.setPort(Integer.parseInt("8080"));
        uri.setPath("v1");
        uri.setPathSegments("v2");
        System.out.println(uri.toString()); */

        APIRequestFactory req = new APIRequestFactory();
        try {
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("before", "2020-06-30T00:00:00.000"));
            params.add(new BasicNameValuePair("after", "2020-01-30T00:00:00.000"));
            HttpMessage msg = req.invokeAPI("", 443, "GET", "v1/orgs/sandbox/environments/service/events", "admin.sandbox@banksinarmas.com", "s4ndb0xS1m4s!", 10000, params);
            System.out.println(msg);
        } catch(IOException | URISyntaxException urie) {
            Utility.printException(urie);
        }
    }

    public static HttpMessage invokeAPI(String hostname, int port, String method, String basepath, String username, String password, int timeout, List<NameValuePair> nvps) throws IOException, URISyntaxException {
        // https://apimgmtdev.banksinarmas.com/v1/orgs/sandbox/environments/service/events

        CloseableHttpClient httpClient = HttpUtil.createHttpsClient("TLS");

        URIBuilder uri = new URIBuilder();
        uri.setScheme("https");
        uri.setHost(hostname);
        uri.setPort(port);
        uri.setPath(basepath);
        if(nvps != null) {
            uri.setParameters(nvps);
        }

        HttpRequestBase httpRequest;
        if(method == "GET") {
            httpRequest = new HttpGet();
        } else {
            httpRequest = new HttpPost();
        }
        
        RequestConfig requestConfig = RequestConfig.custom().
                setConnectionRequestTimeout(timeout).
                setConnectTimeout(timeout).
                setSocketTimeout(timeout).
                build();

        String basicAuthentication = SecurityUtil.encode(username + ":" + password);
        httpRequest.setHeader(HttpHeaders.AUTHORIZATION, "Basic " + basicAuthentication);
        httpRequest.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        httpRequest.setHeader(HttpHeaders.ACCEPT, "application/json");
        HttpUtil.logHeader(httpRequest.getAllHeaders());

        httpRequest.setURI(uri.build());
        httpRequest.setConfig(requestConfig);

        log.info("Request URL : " + httpRequest.getURI().toASCIIString());
        HttpResponse response = httpClient.execute(httpRequest);
        log.info("HTTP Response : HTTP " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());

        // Get Response
        int statusCode = response.getStatusLine().getStatusCode();
        String body = "";
        if(statusCode >= 200 && statusCode < 300) {
            ResponseHandler<String> handler = new BasicResponseHandler();
            body = handler.handleResponse(response);
        }
        HttpMessage responseMsg = new HttpMessage();
        responseMsg.setStatusCode(statusCode);
        responseMsg.setBody(body);
        return responseMsg;
    }



}
