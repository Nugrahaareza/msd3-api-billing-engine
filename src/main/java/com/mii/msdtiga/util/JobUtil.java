package com.mii.msdtiga.util;

import com.mii.msdtiga.entity.ApimConnection;
import com.mii.msdtiga.entity.JobTrail;
import com.mii.msdtiga.service.SystemPropertiesService;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class JobUtil {

    public static List<ApimConnection> getAllApimConnections() {
        JSONArray jsonArray = new JSONArray(SystemPropertiesService.getPropertiesString("apim.connection"));
        List<ApimConnection> conns = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            ApimConnection apimc = new ApimConnection();
            apimc.setUsername(jsonObject.getString("username"));
            apimc.setCatalogName(jsonObject.getString("catalog"));
            apimc.setHostname(jsonObject.getString("hostname"));
            apimc.setOrgName(jsonObject.getString("orgname"));
            apimc.setPort(jsonObject.getInt("port"));
            apimc.setTimeout(jsonObject.getInt("timeout"));
            conns.add(apimc);
        }
        return conns;
    }
    
    public static JobTrail newJobTrail(String jobTriggerSource, String jobName) {
        JobTrail jobTrail = new JobTrail();
        jobTrail.setSource(jobTriggerSource);
        jobTrail.setStartDate(LocalDateTime.now());
        jobTrail.setJobName(jobName);
        return jobTrail;
    }

    public static Trigger createOnDemandTrigger(String triggerKey, String jobName) {
        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity(triggerKey)
                .forJob(jobName)
                .startNow()
                .build();
        return trigger;
    }

    public static Trigger createOnDemandTrigger(String triggerKey) {
        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity(triggerKey)
                .startNow()
                .build();
        return trigger;
    }

    public static void startJob(Trigger trigger) throws SchedulerException {
        SchedulerFactory schedulerFactory = new StdSchedulerFactory();
        Scheduler scheduler = schedulerFactory.getScheduler();
        scheduler.scheduleJob(trigger);
        scheduler.start();
    }

    public static void startJob(JobDetail jobDetail, Trigger trigger) throws SchedulerException, InterruptedException {
        SchedulerFactory schedulerFactory = new StdSchedulerFactory();
        Scheduler scheduler = schedulerFactory.getScheduler();
        scheduler.scheduleJob(jobDetail, trigger);
        scheduler.start();
        Thread.sleep(3000);
        scheduler.deleteJob(jobDetail.getKey());
    }

}
