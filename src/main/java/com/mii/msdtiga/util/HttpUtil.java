package com.mii.msdtiga.util;

import org.apache.http.*;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.List;

public class HttpUtil {

    static final org.slf4j.Logger log = LoggerFactory.getLogger(HttpUtil.class);

    public static final String HTTP_METHOD_GET = "GET";

    public static CloseableHttpClient createHttpsClient(String sslContextString) {
        CloseableHttpClient httpClient = null;
        try {
            SSLContext sslcontext = SSLContext.getInstance(sslContextString);
            sslcontext.init(null, null, null);
            SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(sslcontext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            httpClient = HttpClients.custom().setSSLSocketFactory(sf).build();
            return httpClient;
        } catch (NoSuchAlgorithmException | KeyManagementException ex) {
            Utility.printException(ex);
            if (httpClient != null) {
                try {
                    httpClient.close();
                } catch (IOException ioe) {
                    log.error(Utility.printException(ioe));
                }
            }
            return null;
        }
    }

    public static HttpGet createHttpGet(String url, String relativeUrl, int port, int timeout) {
        if (port == 0 || port == 80) {
            url = url + relativeUrl;
        } else {
            url = url + ":" + port + relativeUrl;
        }
        RequestConfig requestConfig = RequestConfig.custom().
                setConnectionRequestTimeout(timeout).
                setConnectTimeout(timeout).
                setSocketTimeout(timeout).
                build();
        HttpGet httpGet = new HttpGet(url);
        httpGet.setConfig(requestConfig);
        return httpGet;
    }

    public static HttpPost createHttpPost(String url, String relativeUrl, int port, int timeout) {
        if (port == 0 || port == 80) {
            url = url + relativeUrl;
        } else {
            url = url + ":" + port + relativeUrl;
        }
        RequestConfig requestConfig = RequestConfig.custom().
                setConnectionRequestTimeout(timeout).
                setConnectTimeout(timeout).
                setSocketTimeout(timeout).
                build();
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(requestConfig);
        return httpPost;
    }

    public static void logHeader(Header[] headerArray) {
        StringBuilder sb = new StringBuilder();
        sb.append("\nRequest Header").append("\n");
        int i = 0;
        for (Header header : headerArray) {
            sb.append(header.getName())
                    .append(": ")
                    .append(header.getValue());
            if (headerArray.length > 1) {
                if (i < headerArray.length - 1) {
                    sb.append("\n");
                }
            }
            i++;
        }
        log.info(sb.toString());
    }

    public static void logEntity(List<NameValuePair> nameValuePairs) {
        StringBuilder sb = new StringBuilder();
        sb.append("\nRequest query parameter").append("\n");
        int i = 0;
        for (NameValuePair nameValuePair : nameValuePairs) {
            sb.append(nameValuePair.getName())
                    .append("=")
                    .append(nameValuePair.getValue());
            if (nameValuePairs.size() > 1) {
                if (i < nameValuePairs.size() - 1) {
                    sb.append("\n");
                }
            }
            i++;
        }
        log.info(sb.toString());
    }

    public static HttpResponse sendAndRcvHttpDelete(String url, int connectRequestTimeout, int connectTimeout, int socketTimeout) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        SSLContext sslcontext = SSLContext.getInstance("TLS");
        sslcontext.init(null, null, null);
        SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(sslcontext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(connectRequestTimeout).setConnectTimeout(connectTimeout).setSocketTimeout(socketTimeout).build();
        HttpDelete httpDelete = new HttpDelete(url);
        httpDelete.setConfig(requestConfig);
        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sf).build();
        log.info("Send HTTP Delete request to '" + url + "'");
        HttpResponse response = httpClient.execute(httpDelete);
        log.info("Receive HTTP Response from '" + url + "' with Status Code : " + response.getStatusLine().getStatusCode());
        return response;
    }

    public static HttpResponse sendAndRcvHttpPutPlainText(String url, String contentRequest, int connectRequestTimeout, int connectTimeout, int socketTimeout) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        SSLContext sslcontext = SSLContext.getInstance("TLS");
        sslcontext.init(null, null, null);
        SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(sslcontext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(connectRequestTimeout).setConnectTimeout(connectTimeout).setSocketTimeout(socketTimeout).build();
        HttpPut httpPut = new HttpPut(url);
        httpPut.setConfig(requestConfig);
        StringEntity se = new StringEntity(contentRequest);
        se.setContentType("text/plain");
        httpPut.setEntity(se);
        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sf).build();

        HttpResponse response = httpClient.execute(httpPut);

        return response;
    }

    public static HttpResponse sendAndRcvHttpPostPlainText(String url, String contentRequest, int connectRequestTimeout, int connectTimeout, int socketTimeout) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        SSLContext sslcontext = SSLContext.getInstance("TLS");
        sslcontext.init(null, null, null);
        SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(sslcontext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(connectRequestTimeout).setConnectTimeout(connectTimeout).setSocketTimeout(socketTimeout).build();
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(requestConfig);
        StringEntity se = new StringEntity(contentRequest);
        se.setContentType("text/plain");
        httpPost.setEntity(se);

        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sf).build();
        HttpResponse response = httpClient.execute(httpPost);
        return response;
    }

    public static HttpResponse sendAndRcvHttpPostJsonApplicationText(String url, String contentRequest, int connectRequestTimeout, int connectTimeout, int socketTimeout) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        TrustManager[] trustAllCerts = {new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
        };
        SSLContext sslcontext = SSLContext.getInstance("SSL");
        sslcontext.init(null, trustAllCerts, new SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sslcontext.getSocketFactory());
        X509HostnameVerifier allX509HostsValid = new X509HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }

            @Override
            public void verify(String string, SSLSocket ssls)
                    throws IOException {
            }

            @Override
            public void verify(String string, X509Certificate xc)
                    throws SSLException {
            }

            @Override
            public void verify(String string, String[] strings, String[] strings1)
                    throws SSLException {
            }
        };
        SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(sslcontext, allX509HostsValid);
        RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(connectRequestTimeout).setConnectTimeout(connectTimeout).setSocketTimeout(socketTimeout).build();
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(requestConfig);
        StringEntity se = new StringEntity(contentRequest);
        se.setContentType("application/json");
        httpPost.setEntity(se);
        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sf).build();
        HttpResponse response = httpClient.execute(httpPost);
        return response;
    }

    public static HttpResponse sendAndRcvHttpPostForm(String url, List<NameValuePair> nameValuePairs, int connectRequestTimeout, int connectTimeout, int socketTimeout) throws UnsupportedEncodingException, IOException, NoSuchAlgorithmException, KeyManagementException {
        SSLContext sslcontext = SSLContext.getInstance("TLS");
        sslcontext.init(null, null, null);
        SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(sslcontext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(connectRequestTimeout).setConnectTimeout(connectTimeout).setSocketTimeout(socketTimeout).build();
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(requestConfig);
        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sf).build();
        HttpResponse response = httpClient.execute(httpPost);
        return response;
    }

    public static HttpResponse sendAndRcvHttpGet(String url, int connectRequestTimeout, int connectTimeout, int socketTimeout) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        SSLContext sslcontext = SSLContext.getInstance("TLS");
        sslcontext.init(null, null, null);
        SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(sslcontext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(connectRequestTimeout).setConnectTimeout(connectTimeout).setSocketTimeout(socketTimeout).build();
        HttpGet httpGet = new HttpGet(url);
        httpGet.setConfig(requestConfig);

        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sf).build();
        HttpResponse response = httpClient.execute(httpGet);
        return response;
    }

    public static String getStringHttpContent(HttpResponse response) throws IllegalStateException, IOException {
        StringWriter writer = new StringWriter();
        IOUtils.copy(response.getEntity().getContent(), writer);
        String content = writer.toString();
        return content;
    }

    public static String getStringHttpContent(HttpEntity entity) throws IOException {
        StringWriter writer = new StringWriter();
        IOUtils.copy(entity.getContent(), writer);
        String content = writer.toString();
        return content;
    }

    /*public static void main(String[] args) {
        log.info("Application test started....!");
        CloseableHttpClient httpClient = null;
        try {
            String basePath = Utility.getSystemProperties("apim.basepath");
            String organizationName = Utility.getSystemProperties("apim.orgprovider.name");
            //List<NameValuePair> a = new ArrayList<>();
            //a.add(new BasicNameValuePair("grant_type", Utility.getSystemProperties("bca.grant.type")));
            //a.add(new BasicNameValuePair("grant_type", "client_credentials"));

            int reqTimeout = Integer.parseInt(Utility.getSystemProperties("apim.request.timeout"));
            SSLContext sslcontext = SSLContext.getInstance("TLS");
            sslcontext.init(null, null, null);
            SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(sslcontext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            //RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(30000).setConnectTimeout(30000).setSocketTimeout(30000).build();
            RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(reqTimeout).setConnectTimeout(reqTimeout).setSocketTimeout(reqTimeout).build();
            HttpGet httpGet = new HttpGet(basePath + "/" + organizationName);
            httpGet.setConfig(requestConfig);
            //httpGet.setEntity(new UrlEncodedFormEntity(a));

            String username = Utility.getSystemProperties("apim.username");
            String password = Utility.getSystemProperties("apim.password");
            String encodeResult = SecurityUtil.encode(username + ":" + password);

            httpGet.setHeader(HttpHeaders.AUTHORIZATION, "Basic " + encodeResult);
            //httpGet.setHeader(HttpHeaders.AUTHORIZATION, "Basic N2RkZTUxY2YtOGE4Ni00NjAxLWIzY2ItZDE1NmM4MjVlODRlOmQxNTZmY2M2LTliYzctNGYzYy05YTk2LWU3Yjc0MmY2MmJkOQ==");
            httpGet.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");

            httpClient = HttpClients.custom().setSSLSocketFactory(sf).build();
            //httpClient = HttpClients.custom().build();
            HttpResponse response = httpClient.execute(httpGet);

            ResponseHandler<String> handler = new BasicResponseHandler();
            String body = handler.handleResponse(response);

            log.info("Response Message : " + body);

            int code = response.getStatusLine().getStatusCode();
            log.info("Code : " + code);

        } catch (IOException | NoSuchAlgorithmException | KeyManagementException ex) {
            try {
                if (httpClient != null) {
                    httpClient.close();
                }
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                ex.printStackTrace(printWriter);
                log.error(printWriter.toString());
            } catch (IOException ex1) {
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                ex1.printStackTrace(printWriter);
                log.error(printWriter.toString());
            }
        }

    }*/

}
