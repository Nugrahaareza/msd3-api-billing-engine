package com.mii.msdtiga.util;

import javax.crypto.*;
import javax.crypto.spec.DESedeKeySpec;
import java.io.*;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

public class Utility {

    static ArrayList modules = new ArrayList();
    static final String Encoding = "ISO8859_1";
    static final int PadLeft = 0;
    static final int PadRight = 1;

    public static String[] sortArray(String[] array) {
        Arrays.sort(array);
        return array;
    }

    public static String specialEncode(String s) throws UnsupportedEncodingException {
        String[] a = s.split("");
        String newString = "";
        for (int i = 0; i < a.length; i++) {
            if (a[i].matches("[a-zA-Z_0-9]")) {
                newString = newString + a[i];
            } else if (a[i].matches("[ \\t\\n\\x0b\\r\\f]")) {
                newString = newString + "%20";
            } else {
                switch (a[i]) {
                    case "/":
                        newString = newString + a[i];
                        break;
                    case "?":
                        newString = newString + a[i];
                        break;
                    case "=":
                        newString = newString + a[i];
                        break;
                    case "&":
                        newString = newString + a[i];
                        break;
                    case "-":
                        newString = newString + a[i];
                        break;
                    case "_":
                        newString = newString + a[i];
                        break;
                    case ".":
                        newString = newString + a[i];
                        break;
                    case "~":
                        newString = newString + a[i];
                        break;
                    default:
                        newString = newString + URLEncoder.encode(a[i], "UTF-8");
                        break;
                }
            }
        }
        return newString;
    }

    public static String printException(Exception ex) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        ex.printStackTrace(printWriter);
        return stringWriter.toString();
    }

    public static double generateRandomDouble(double rangeStart, double rangeEnd) {
        return rangeStart + new Random().nextDouble() * (rangeStart - rangeEnd);
    }

    public static String convertDoubleToString(double d, String format) {
        DecimalFormat decimalFormat = new DecimalFormat(format);
        String numberAsString = decimalFormat.format(d);
        return numberAsString;
    }

    public static String formatAmount(double val) {
        String ret = "";
        DecimalFormat decFrm = new DecimalFormat();
        decFrm.applyPattern("##,###,###,###");
        ret = decFrm.format(val).replace(',', '.');
        return ret;
    }

    public static int roundedUp(double val) {
        String sAmount = stringAmount(val);
        String last2Digit = sAmount.substring(sAmount.length() - 2, sAmount.length());
        if (!last2Digit.equals("00")) {
            int intLastDigit = Integer.parseInt(last2Digit);
            int margin = 100 - intLastDigit;
            int roundedAmount = Integer.parseInt(sAmount) + margin;
            return roundedAmount;
        } else {
            return Integer.parseInt(sAmount);
        }

    }

    public static String stringAmount(double val) {
        DecimalFormat decFrm = new DecimalFormat();
        decFrm.applyPattern("#");
        String ret = decFrm.format(val);
        return ret;
    }

    public static String stringAmount(BigDecimal val) {
        val.setScale(0, 1);
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(0);
        df.setGroupingUsed(false);
        return df.format(val);
    }

    public static String hash(String s, String algorithm) {
        String ret = "";
        try {
            MessageDigest msg = MessageDigest.getInstance(algorithm);
            msg.update(s.getBytes());
            byte[] hash = msg.digest();
            //ret = ISOUtil.hexString(hash);
            ret = null;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return ret;
    }

    public static int charCount(String s, String ch) {
        int ret = 0;
        int len = s.length();
        for (int i = 0; i < len; i++) {
            if (ch.equalsIgnoreCase(s.substring(i, i + 1))) {
                ret++;
            }
        }
        return ret;
    }

    public static String getUUID() {
        UUID gen = UUID.randomUUID();
        String uuid = gen.toString();
        uuid = uuid.replace("-", "");
        uuid = uuid.toUpperCase();
        return uuid;
    }

    public static String generateRandomNumber(int length) {
        UUID gen = UUID.randomUUID();
        long randomNumber = Math.abs(gen.getLeastSignificantBits());
        String stringRep = String.valueOf(randomNumber);
        String reff1 = stringRep.substring(0, length);
        return reff1;
    }

    public byte[] decrypt(String path, String plaintext)
            throws IllegalBlockSizeException, BadPaddingException, NoSuchPaddingException, InvalidKeySpecException, InvalidKeyException, NoSuchAlgorithmException, IOException, InvalidKeyException {
        DataInputStream in = new DataInputStream(new FileInputStream(path));
        byte[] rawkey = new byte[path.length()];
        in.readFully(rawkey);
        in.close();

        DESedeKeySpec keyspec = new DESedeKeySpec(rawkey);
        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("DESede");
        SecretKey key = keyfactory.generateSecret(keyspec);
        Cipher cipher = Cipher.getInstance("DES");
        byte[] ciphertext = cipher.doFinal(plaintext.getBytes());
        cipher.init(2, key);
        byte[] plaindecrypt = cipher.doFinal(ciphertext);
        return plaindecrypt;
    }

    public static void waitUntil(int finish) throws InterruptedException {
        int c = 0;

        while (c < finish) {
            Thread.sleep(1000L);
            c++;
        }
    }

    public static byte[] hex2Byte(String h) {
        if (h.length() % 2 != 0) {
            h = "0" + h;
        }

        int l = h.length() / 2;

        byte[] r = new byte[l];

        int i = 0;
        int j = 0;
        for (int k = h.length(); i < k; j++) {
            r[j] = Short.valueOf(h.substring(i, i + 2), 16).byteValue();

            i += 2;
        }

        return r;
    }

    public static String hex2String(String h) {
        return new String(hex2Byte(h));
    }

    public static String byte2Hex(byte[] b) {
        StringBuffer sbuf = new StringBuffer();

        int i = 0;
        for (int n = b.length; i < n; i++) {
            byte hiByte = (byte) ((b[i] & 0xF0) >> 4);
            byte loByte = (byte) (b[i] & 0xF);

            sbuf.append(Character.forDigit(hiByte, 16));
            sbuf.append(Character.forDigit(loByte, 16));
        }

        return sbuf.toString();
    }

    public static String String2Hex(String s) {
        return byte2Hex(s.getBytes());
    }

    public static String str2bcd(int argInt) {
        try {
            ByteArrayOutputStream o = new ByteArrayOutputStream();
            o.write(argInt >> 8);
            o.write(argInt);

            return o.toString("ISO8859_1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return "0";
    }

    public static int bcd2str(String argInt) {
        try {
            byte[] b = argInt.getBytes("ISO8859_1");

            return (b[0] & 0xFF) << 8 | b[1] & 0xFF;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public static int bcd2str(byte[] argInt) {
        try {
            return (argInt[0] & 0xFF) << 8 | argInt[1] & 0xFF;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public static String pad(String m, int pos, String with, int total) {
        if (m.length() < total) {
            StringBuffer s = new StringBuffer(m);

            while (s.length() < total) {
                if (pos == 0) {
                    s.insert(0, with);
                    continue;
                }
                if (pos != 1) {
                    continue;
                }
                s.append(with);
            }

            return s.toString();
        }

        if (pos == 0) {
            return m.substring(m.length() - total);
        }
        if (pos == 1) {
            return m.substring(0, total);
        }

        return m.substring(0, total);
    }

    public static String pad(String m, String pos, String with, int total) {
        if (m.length() < total) {
            StringBuffer s = new StringBuffer(m);

            while (s.length() < total) {
                if ("L".equals(pos)) {
                    s.insert(0, with);
                    continue;
                }
                if ("R".equals(pos)) {
                    s.append(with);
                }
            }
            return s.toString();
        }

        if ("L".equals(pos)) {
            return m.substring(m.length() - total);
        }
        if ("R".equals(pos)) {
            return m.substring(0, total);
        }

        return m.substring(0, total);
    }

    public static String decode7bit(String src) {
        String result = null;
        String temp = null;
        byte left = 0;

        if ((src != null) && (src.length() % 2 == 0)) {
            result = "";
            int[] b = new int[src.length() / 2];
            temp = src + "0";

            int i = 0;
            int j = 0;
            for (int k = 0; i < temp.length() - 2; j++) {
                b[j] = Integer.parseInt(temp.substring(i, i + 2), 16);

                k = j % 7;
                byte srcAscii = (byte) (b[j] << k & 0x7F | left);
                result = result + (char) srcAscii;
                left = (byte) (b[j] >>> 7 - k);

                if (k == 6) {
                    result = result + (char) left;
                    left = 0;
                }
                if (j == src.length() / 2) {
                    result = result + (char) left;
                }
                i += 2;
            }
        }

        return result.trim();
    }

    public static Boolean compareDate(Date date1, Date date2) {
        SimpleDateFormat sdf = new SimpleDateFormat("ddmmyy");
        String dateString1 = sdf.format(date1);

        String dateString2 = sdf.format(date2);
        if (dateString1.equals(dateString2)) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public static boolean isValidValue(Pattern pattern, String value) {
        return pattern.matcher(value).matches();
    }

//    public static void setSystemProperties(String key, String value) {
//        InputStream input = null;
//        try {
//            input = new FileInputStream("application.properties");
//
//            Properties prop = new Properties();
//            prop.load(input);
//
//            prop.setProperty(key, value);
//        } catch (IOException ex) {
//            if (input != null) {
//                try {
//                    input.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }

//    public static String getSystemProperties(String properties) {
//        Properties prop = new Properties();
//        InputStream input = null;
//        try {
//            String filePath = "application.properties";
//
//            input = new FileInputStream(filePath);
//            // load a properties file
//            prop.load(input);
//
//            HashMap mymap = new HashMap();
//            for (String key : prop.stringPropertyNames()) {
//                String value = prop.getProperty(key);
//                mymap.put(key, value);
//            }
//
//            // get the property value and print it out
//            return prop.getProperty(properties);
//        } catch (IOException ex) {
//            ex.printStackTrace();
//            return null;
//        } finally {
//            if (input != null) {
//                try {
//                    input.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }

}
