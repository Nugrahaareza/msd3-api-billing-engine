package com.mii.msdtiga.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class DateUtil {

    public static List<Date> getDatesBetween(
            Date startDate, Date endDate) {
        List<Date> datesInRange = new ArrayList<>();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startDate);

        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(endDate);
        
        endCalendar.add(Calendar.DATE, 1);

        while (calendar.before(endCalendar)) {
            Date result = calendar.getTime();
            datesInRange.add(result);
            calendar.add(Calendar.DATE, 1);
        }
        return datesInRange;
    }

    public static String getDate(String format) {
        String ret = "";
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(format);
        long t = System.currentTimeMillis();
        try {
            Date dateadd = new Date(t);
            cal.setTime(dateadd);
            ret = df.format(dateadd);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public static Date getYesterdayDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(5, -1);

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }
    
    public static LocalDate getYesterdayLocalDate() {
        LocalDate localDateNow = LocalDate.now();
        return localDateNow.minusDays(1);
    }
    
    public static int getMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.MONTH);
    }
    
    public static int getYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.YEAR);
    }

    public static Date getLastMonth() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    public static Date getLastMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, -1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    public static Date getTodayDate() {
        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    public static String getCurrentDateTime() {
        Date now = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        String datetime = df.format(now);
        return datetime;
    }

    public static Date getDateFromStr(String s, String pattern) {
        Date datetime = null;
        try {
            SimpleDateFormat df = new SimpleDateFormat(pattern);

            datetime = df.parse(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return datetime;
    }

    public static String getStrFromDate(Date dt, String pattern) {
        String ret = null;
        try {
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            ret = df.format(dt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public static String getSettleDate(String date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String ret = "";

        if (date == null) {
            return ret;
        }

        if (date.equals("00000000")) {
            return ret;
        }
        try {
            df.parse(date);
            ret = date;
        } catch (Exception e) {
            e.printStackTrace();
            ret = "";
        }
        return ret;
    }

    public static Date convertLocalDateToDate(LocalDate localDate) {
        ZoneId defaultZoneId = ZoneId.systemDefault();
        Date date = Date.from(localDate.atStartOfDay(defaultZoneId).toInstant());
        return date;
    }

    public static LocalDateTime convertDateToLocalDateTime(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }

    public static LocalDate convertDateToLocalDate(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    public static String formatDate(String date, String fromFormat, String toFormat) {
        SimpleDateFormat src = new SimpleDateFormat();
        SimpleDateFormat dst = new SimpleDateFormat();

        if ((date == null) || (date.trim().length() == 0)) {
            return "";
        }

        if ((fromFormat == null) || (fromFormat.trim().length() == 0) || (toFormat == null) || (toFormat.trim().length() == 0)) {
            return null;
        }
        try {
            src.applyPattern(fromFormat);
            dst.applyPattern(toFormat);
            String result = dst.format(src.parse(date));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

        System.out.println("Yesterday : " + dateFormat.format(DateUtil.getTodayDate()));
    }

}
