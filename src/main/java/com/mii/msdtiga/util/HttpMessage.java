package com.mii.msdtiga.util;

public class HttpMessage {

    private int statusCode;
    private String body;


    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String toString() {
        return "Response Body : " + body + "\n" + " Status Code : " + statusCode;
    }
}
