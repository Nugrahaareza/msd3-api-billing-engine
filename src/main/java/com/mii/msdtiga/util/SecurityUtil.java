package com.mii.msdtiga.util;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jpos.iso.ISOUtil;
import org.slf4j.LoggerFactory;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Base64;
import javax.xml.bind.DatatypeConverter;

public class SecurityUtil {

    static final org.slf4j.Logger log = LoggerFactory.getLogger(SecurityUtil.class);

    // Encrypt Algorithm
    public static String Encrypt_PBEWithMD5AndTripleDES = "PBEWithMD5AndTripleDES";
    public static String Encrypt_AES = "AES";
    public static String Encrypt_Blowfish = "Blowfish";
    public static String Encrypt_DES = "DES";
    public static String Encrypt_DESede = "DESede";
    public static String Encrypt_DiffieHellman = "DiffieHellman";
    public static String Encrypt_OAEP = "OAEP";
    public static String Encrypt_PBEWithMD5AndDES = "PBEWithMD5AndDES";
    public static String Encrypt_PBEWithSHA1AndDESede = "PBEWithSHA1AndDESede";
    public static String Encrypt_PBEWithSHA1AndRC2_40 = "PBEWithSHA1AndRC2_40";
    public static String Encrypt_RC2 = "RC2";

    // Hash Message Authentication Code
    public static String Hmac_MD5 = "HmacMD5";
    public static String Hmac_SHA1 = "HmacSHA1";
    public static String Hmac_SHA256 = "HmacSHA256";
    public static String Hmac_SHA384 = "HmacSHA384";
    public static String Hmac_SHA512 = "HmacSHA512";

    // Hash Algorithm
    public static String Hash_SHA256 = "SHA-256";
    public static String Hash_SHA1 = "SHA-1";
    public static String Hash_MD5 = "MD5";

    public static String decrypt(String encryptedText, String key, String keyHashAlgorithm, String encryptAlgorithm, String outputType) {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        if (keyHashAlgorithm == null || keyHashAlgorithm.trim().isEmpty()) {
            encryptor.setPassword(key);
        } else {
            encryptor.setPassword(Utility.hash(key, keyHashAlgorithm));
        }
        encryptor.setAlgorithm(encryptAlgorithm);
        encryptor.setStringOutputType(outputType);
        return encryptor.decrypt(encryptedText);
    }

    public static String encrypt(String plainText, String key, String keyHashAlgorithm, String encryptAlgorithm, String outputType) {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        if (keyHashAlgorithm == null || keyHashAlgorithm.trim().isEmpty()) {
            encryptor.setPassword(key);
        } else {
            encryptor.setPassword(Utility.hash(key, keyHashAlgorithm));
        }
        encryptor.setAlgorithm(encryptAlgorithm);
        encryptor.setStringOutputType(outputType);
        return encryptor.encrypt(plainText);
    }

    public static void main(String[] args) {
        try {
            //StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
            //encryptor.setPassword("BPRKS123456ABCDE");                     // we HAVE TO set a password
            //encryptor.setAlgorithm(Encrypt_DESede);    // optionally set the algorithm
            //encryptor.setStringOutputType("base64");
            //String encryptedText = encryptor.encrypt("6278791575700001");
            //String plainText = encryptor.decrypt("DU9h/FqQmR/GwnohtBhetA==");  // myText.equals(plainText)
            //System.out.println("Encrypted : " + encryptedText);
            //System.out.println("Plain : " + plainText);

            String text = "";
            System.out.println(hash(text, SecurityUtil.Hash_SHA256).toLowerCase());
        } catch (NoSuchAlgorithmException ex) {
            log.error("No Such Algorithm Exception", ex);
        }


    }

    public static String encode(String s) {
        byte[] encodedBytes = Base64.getEncoder().encode(s.getBytes());
        return new String(encodedBytes);
    }

    public static String encode(byte[] encodeBytes) {
        byte[] encodedBytes = Base64.getEncoder().encode(encodeBytes);
        return new String(encodedBytes);
    }

    public static String decode(String s) {
        byte[] decodedBytes = Base64.getDecoder().decode(s);
        return new String(decodedBytes);
    }

    /**
     * Get Hash value of string s
     *
     * @param s
     * @return ret
     */
    public static String hash(String s, String hashAlgorithm) throws NoSuchAlgorithmException {
        String ret = "";
        MessageDigest msg = MessageDigest.getInstance(hashAlgorithm);
        msg.update(s.getBytes());
        byte[] hash = msg.digest();
        ret = ISOUtil.hexString(hash);
        return ret;
    }

    /**
     * Computes RFC 2104-compliant HMAC signature.
     *
     * * @param data The data to be signed.
     * @param data
     * @param hmac
     * @param key The signing key.
     * @param resultType
     * @return The Base64-encoded RFC 2104-compliant HMAC signature.
     * @throws java.security.SignatureException when signature generation fails
     */
    public static String calculateRFC2104HMAC(String data, String hmac, String key, String resultType) throws java.security.SignatureException {
        String result;
        try {
            // get an hmac_sha1 key from the raw key bytes
            SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), "HmacSHA1");
            // get an hmac_sha1 Mac instance and initialize with the signing key
            Mac mac = Mac.getInstance(hmac);
            mac.init(signingKey);
            // compute the hmac on input data bytes
            byte[] rawHmac = mac.doFinal(data.getBytes());
            // base64-encode the hmac

            if ("hexadecimal".equals(resultType)) {
                result = ISOUtil.hexString(rawHmac);
            } else {
                result = DatatypeConverter.printBase64Binary(rawHmac);
            }

        } catch (Exception e) {
            throw new SignatureException("Failed to generate HMAC : " + e.getMessage());
        }
        return result;
    }

    public static String calculateHMAC256(String data, String key, String resultType) {
        String result;
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(key.getBytes(), "HmacSHA256");
            mac.init(secret_key);
            byte[] rawHmac = mac.doFinal(data.getBytes());

            if ("hexadecimal".equals(resultType)) {
                result = ISOUtil.hexString(rawHmac);
            } else {
                result = DatatypeConverter.printBase64Binary(rawHmac);
            }
            return result;
        } catch (IllegalStateException | InvalidKeyException | NoSuchAlgorithmException e) {
            log.error("Exception", e);
            return null;
        }
    }

}
