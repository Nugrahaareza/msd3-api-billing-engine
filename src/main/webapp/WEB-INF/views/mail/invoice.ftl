<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  </head>
  <body>
    <p>Hi ${recipientName}</p>
    <p>Here attached your invoice.</p>
    <p>Best Regards,</p>
    <p>${senderName}</p>
    <p></p>
  </body>
</html>
