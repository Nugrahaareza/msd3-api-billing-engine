<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  </head>
  <body>
    <p>Hi ${recipientName}</p>
    <p>Your invoice are due in ${dueDateCount} days, please do the payment. Please ignore this message if already paid</p>
    <p>Best Regards,</p>
    <p>${senderName}</p>
    <p></p>
  </body>
</html>